library(GenomicRanges)
library(metagene2)
library(ggplot2)

source("scripts/utils.R")

danpos_output="output/danpos_input_auto/2019-10-09GMT"
FLANK_SIZE=750

# Get bam files.
design = load_design_combined()

# Load the +1 dyads.
dyad_files = c(Sys.glob(file.path(danpos_output, "/*.txt.plus_1_dyads.tsv")))
names(dyad_files) = paste0("DANPOS_", gsub(".txt.plus_1_dyads.tsv", "", basename(dyad_files)))
dyad_files["Brogaard"] = "input/nucleosome_sacCer3.txt.plus_1_dyads.tsv"
#dyad_files["Ocampo_WT_30"] = "output/WT_30C_plus_one_Ocampo.txt"
ndr_dyads = Sys.glob("output/NDR/dyads/*.txt")
names(ndr_dyads) = paste0(gsub(".txt", "", basename(ndr_dyads)))

dyad_files = c(dyad_files, ndr_dyads)

chr_sizes = get_chr_sizes()
all_dyads = lapply(dyad_files, function(x) {
    input_table = read.table(x, sep="\t", header=TRUE)
    if(!is.null(input_table$geneStrand)) {
        input_table$strand = ifelse(input_table$geneStrand==1, "+", "-")
    }
    
	if(is.null(input_table$dyad)) {
		input_table$dyad = input_table$start
	} else {
		input_table$start = input_table$dyad
		input_table$end = input_table$dyad
		input_table$width = 1
	}
	
	input_table$MaxEnd = chr_sizes[as.character(input_table$seqnames)]
    
	input_table = input_table[!is.na(input_table$start),]
	
    input_gr = GRanges(input_table)
    flanked_gr = GenomicRanges::flank(input_gr, FLANK_SIZE, both=T)
    flanked_gr = flanked_gr[end(flanked_gr) < flanked_gr$MaxEnd]
        
    flanked_gr
})

#USE_REF="WT"
#USE_REF="SELF"
#USE_REF="Brogaard"
design$RegionRef = design$Ref
USE_REF="design"
#USE_REF="Ocampo_WT_30"
if(USE_REF=="SELF") {
    design$RegionRef = design$Label
} else if(USE_REF=="design") {
	# Do nothing.
} else {
    design$RegionRef = USE_REF
} 

#dir.create("output/metagene/cache_ocampo_wt30", recursive=TRUE)
CACHE_PATH = "output/NDR/metagene/cache_ocampo_ocampo"
dir.create(CACHE_PATH, recursive=TRUE, showWarnings=FALSE)
mg_list = list()
for(i in unique(design$Label)) {
    design_subset = design[design$Label == i,]
    ref_regions = all_dyads[[ design_subset$RegionRef[1] ]]
    #ref_regions$DistPlusToMinusOne = abs(ref_regions$plus_1 - ref_regions$minus_1)
    #ref_regions$DistanceCategory = ifelse(ref_regions$DistPlusToMinusOne <= 220, "Close", "Far")
    
	ref_regions = ref_regions[start(ref_regions) > 0]
	
    cache_file = paste0(CACHE_PATH, i, ".RData")
    if(!file.exists(cache_file)) {
        mg = metagene2::metagene2$new(bam_files = design_subset$BAM,
                                      regions=ref_regions, 
                                      paired_end=TRUE,
                                      normalization="RPM", 
                                      bin_count=1500,
                                      cores=SnowParam(4),
                                      verbose=TRUE)
        mg$produce_metagene(design_metadata=design_subset)
        save(mg, file=cache_file)
    } else {
        load(cache_file)
    }
    
    pdf(file.path("output/NDR/metagene", paste0(i, ".pdf")))
    print(mg$produce_metagene(design_metadata=design_subset))
    dev.off()
    
    mg_list[[i]] = mg    
}

