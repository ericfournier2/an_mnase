#!/bin/bash
mkdir -p output/jobs
mkdir -p output/pipeline/tracks/MNaseBigWig

for i in output/pipeline/alignment/*/*.sorted.dup.bam
do
    script=output/jobs/$sample.MNase_bigwig.sh
    sample=`basename $i .sorted.dup.bam`

cat <<EOF > $script
#!/bin/bash
module load mugqic/deepTools/2.5.3
bamCoverage --bam $i --outFileName output/pipeline/tracks/MNaseBigWig/$sample.bw --normalizeUsingRPKM --MNase --binSize 1 -p 4  
EOF

sbatch -D `pwd` --time 4:00:00 --mem 32G --cpus-per-task 4 --account def-amnou -o $script.stdout -e $script.stderr $script
done
