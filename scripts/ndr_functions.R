require(GenomicRanges)             
require(ggplot2)

# Given a rolling mean of coverages, return a data-frame of nucleosome peaks 
# (negate=FALSE) or valleys (negate=TRUE).
get_peak_df = function(rolled_mean, negate=FALSE, cutoff=-1e60) {
    # Identify peaks.
    peak_info = pracma::findpeaks(rolled_mean, minpeakdistance = 100, 
                                  minpeakheight = cutoff)

    # findpeaks will return NULL if it does not identify any peaks.
    if(is.null(peak_info)) {
        return(NA)
    }
    
    # Convert the matrix output of findpeaks into a data.frame.
    res = as.data.frame(peak_info)
    colnames(res) = c("value", "peak_summit", "peak_start", "peak_end")
    
    # Look at the values 30nt up and downstream to identify potential artifacts.
    res$upstream = rolled_mean[pmax(1, res$peak_summit - 30)]
    res$downstream = rolled_mean[pmin(length(rolled_mean), res$peak_summit + 30)]
    
    # Set peak categories.
    res$category = ifelse(negate, "Valley", "Peak")
    real_summit = (res$upstream < res$value) & (res$downstream < res$value)
    res$Artifact = !real_summit

    # Renegate the values when finding valleys.
    if(negate) {
        res$value = -res$value
    }
    
    # Set the display category and reorder peaks sequentially along the gene.
    res$display_category = paste0(res$category, "-", ifelse(res$Artifact, "Artifact", "Good"))
    res = res[order(res$peak_summit),]
    
    res
}

# From the set of all NDR candidates, select the best one.
choose_best_ndr = function(ndr_df, max_downstream=200) {
    # Group the NDR candidates into categories explaining why they were
    # rejected or chosen.
    ndr_df$Category = "Candidate"
    ndr_df$Category[ndr_df$DistToTSS >= max_downstream] = "Too far downstream"
    ndr_df$Category[ndr_df$Width < 190] = "Too short"
    
    candidates_left = ndr_df$Category == "Candidate"
    if(sum(candidates_left) == 1) {
        # If we have just one candidate, that's the best one.
        ndr_df$Category[candidates_left] = "Best"
    } else if(sum(candidates_left) > 1) {
        # If we have more than one candidate, we look for those which have their
        # centers within 200bp of the TSS.
        close_to_TSS = abs(ndr_df$DistToTSS) <= 200
        close_candidates = candidates_left & close_to_TSS
        if(sum(close_candidates) >= 1) {
            # If more than one NDR candidate is close to the TSS, pick the one
            # with the best score.
            ndr_df$Category[close_candidates] = "CloseToTSS"
            
            min_score = min(ndr_df$Score[close_candidates])
            ndr_df$Category[which(close_candidates & ndr_df$Score == min_score)[1]] = "Best"
        } else {
            # If we have no close candidate, pick the NDR with the best score
            # amongst all. This differs from Ocampo, where they pick the one
            # closest to the TSS.
            min_score = min(ndr_df$Score[candidates_left])
            ndr_df$Category[which(candidates_left & ndr_df$Score == min_score)[1]] = "Best"
        }
    }
    
    ndr_df
}

# Given a set of peaks, determine which inter-peak intervals are NDR candidates.
get_ndr_df = function(peak_df, rolled_mean, max_downstream = 200) {
    # Remove artifact peaks from the set of peaks we'll reason on.
    peak_df = peak_df[!peak_df$Artifact,]
    peak_df = peak_df[order(peak_df$peak_summit),]

    # If we have zero or one peak, we can't find an NDR.
    if(nrow(peak_df) <= 1) {
        return(NA)
    }

    # Build the base positional info for all NDR candidates.
    ndr_df = data.frame(N1=1:(nrow(peak_df) - 1), 
                        N2=2:nrow(peak_df))
    ndr_df$N1Pos = peak_df$peak_summit[ndr_df$N1]
    ndr_df$N2Pos = peak_df$peak_summit[ndr_df$N2]
    ndr_df$PeakMean = (peak_df$value[ndr_df$N1]  + peak_df$value[ndr_df$N2]) / 2
    ndr_df$GapPos = floor((ndr_df$N1Pos + ndr_df$N2Pos) / 2)
    ndr_df$GapValue = rolled_mean[ndr_df$GapPos]
    ndr_df$Score = ndr_df$GapValue / ndr_df$PeakMean
    ndr_df$Width = ndr_df$N2Pos - ndr_df$N1Pos
    ndr_df$DistToTSS = ndr_df$GapPos - floor(length(rolled_mean) / 2)

    # Identify the best NDR and return the data-frame.
    choose_best_ndr(ndr_df, max_downstream)
}

# Converts a relative position in the roled_mean array to a genomic one.
relative_to_genomic <- function(pos, tx_info) {
    if(as.logical(strand(tx_info) == "-")) {
        return(end(tx_info) - 50 - pos)
    } else {
        return(start(tx_info) + 50 + pos)
    }
}

# Returns the relative position of the peak with the given offset from the +1
# nucleosomes.
peak_plus_x = function(offset, peak_df, best_index) {
    if(nrow(peak_df) >= (best_index + offset)) {
        return(peak_df$peak_summit[best_index + offset])
    } else {
        return(NA)
    }
}

GAUSSIAN_MIN=-200
GAUSSIAN_MAX=600
GAUSSIAN_RANGE = seq(GAUSSIAN_MIN, GAUSSIAN_MAX)

# Sum up gaussian distributions to model a nucleosome array.
# Found in supplemental materials from Ocampo, 2016
P_ocampo = function(d, input_range=GAUSSIAN_RANGE) {
    return(dnorm(input_range, mean=0*d, sd=40) +
           dnorm(input_range, mean=1*d, sd=40) +
           dnorm(input_range, mean=2*d, sd=40) +
           dnorm(input_range, mean=3*d, sd=40) +
           dnorm(input_range, mean=4*d, sd=40))
}

# Scales teh results of the gaussian sum so that it has the same amplitude 
# as max_val.
P_ocampo_scaled = function(d, max_val, input_range=GAUSSIAN_RANGE) {
    tmp=P_ocampo(d, input_range)
    tmp * max_val / max(tmp)
}

# Given the peaks and NDR candidates information, calculate relevant metrics
# for each gene (nucleosome position, average spacing, shift, etc.
build_gene_info <- function(peak_df, ndr_df, tx_info, rolled_mean) {
    # If we didn't identify a NDR, we don't have anything to do.
    best_ndr = which(ndr_df$Category=="Best")
    if(length(best_ndr)==0) {
        return(NA)
    }
    
    # Get the relative start and end of the NDR.
    minus_one = ndr_df$N1Pos[best_ndr]
    plus_one = ndr_df$N2Pos[best_ndr]
    
    # Get the relative and genomic coordinates of the +2-+5 nucleosomes.
    best_peak = which(peak_df$peak_summit == plus_one)
    peaks_relative = unlist(lapply(1:4, peak_plus_x, peak_df=peak_df, best_index=best_peak))
    peaks_genomic = unlist(lapply(peaks_relative, relative_to_genomic, tx_info=tx_info))
    
    # Add the nucleosome positions to the GRanges object representing the gene.
    tx_info$plus_one_value = peak_df$value[best_peak]
    tx_info$minus_one = relative_to_genomic(minus_one, tx_info)
    tx_info$plus_one = relative_to_genomic(plus_one, tx_info)
    tx_info$plus_two = peaks_genomic[1]
    tx_info$plus_three = peaks_genomic[2]
    tx_info$plus_four = peaks_genomic[3]
    tx_info$plus_five = peaks_genomic[4]
    
    # Estimate nucleosome distance using GLM
    glm_x = c(0, (1:4)[!is.na(peaks_relative)])
    glm_y = c(plus_one, peaks_relative[!is.na(peaks_relative)]) - plus_one
    tx_info$d_glm = stats::glm.fit(glm_x, glm_y)$coefficients
    
    # Compare to Gaussian distribution to evaluate shift.
    if(!is.na(tx_info$d_glm)) {
        # First off, determine what's the valid range for the comparison.
        # By default we inspect from -200 to +600 from the plus_one dyad.
        ccf_range = seq(plus_one+GAUSSIAN_MIN, plus_one+GAUSSIAN_MAX)
        valid_indices = ccf_range >= 1 & ccf_range <= length(rolled_mean)
        ccf_range = ccf_range[valid_indices]
    
        real_values = rolled_mean[ccf_range]
        gaussian_values = P_ocampo_scaled(tx_info$d_glm, tx_info$plus_one_value)[valid_indices]
        # Diagnostic plot
        if(FALSE) {
            plot_df = data.frame(x=c(ccf_range, ccf_range),
                                 y=c(real_values, gaussian_values),
                                 type=rep(c("Real", "Gaussian"), each=length(ccf_range)))
            print(ggplot(plot_df, aes(x=x, y=y, color=type)) + geom_point())
        }
    
        # Perform the cross-correlation.
        ccf_res = ccf(real_values, gaussian_values, plot=FALSE, lag.max = 75)
        tx_info$lag = ccf_res$lag[which.max(ccf_res$acf)]
    } else {    
        tx_info$lag = NA
    }
    return(as.data.frame(tx_info))
}

# Generate a diagnostic plot of the NDR detection algorithm for a given gene.
plot_gene_ndr = function(coverage, rolled_mean, peak_df, valley_df, ndr_df, mean_cutoff, tx_info, label="unknown", outdir="output") {
    # Build long-form data-frames for the raw data and rolling means.
    raw_df = data.frame(x=1:1901, y=coverage[50:1950], type="Raw")
    mean_df = data.frame(x=1:1901, y=rolled_mean, type="RollMean")
    
    # Build a data-frame for the fitted gaussian distribution.
    # NOTE: Won't work if gaussian fit was truncated because plus_one + GAUSSIAN_RANGE over/underflowed
    ocampo_res = P_ocampo_scaled(tx_info$d_glm, tx_info$plus_one_value)
    gaussian_df = data.frame(x=GAUSSIAN_RANGE+tx_info$plus_one - tx_info$start - 50, y=ocampo_res)
    
    annotation_text = sprintf("d_glm: %.0f\n+1: %.0f", tx_info$d_glm, tx_info$plus_one)
    
    gg_obj = ggplot() +
        geom_line(data=raw_df, mapping=aes(x=x, y=y), color="grey") +
        geom_line(data=mean_df, mapping=aes(x=x, y=y)) +
#        geom_line(data=gaussian_df, mapping=aes(x=x, y=y), color="blue") +
        geom_point(data=rbind(peak_df, valley_df), 
                   mapping=aes(x=peak_summit, y=value, shape=display_category), 
                   size=3, fill="darkgreen") +
        geom_segment(data=ndr_df, 
                     mapping=aes(x=N1Pos, xend=N2Pos, y=GapValue, yend=GapValue, 
                                 color=Category),
                     size=2) +
        geom_hline(yintercept=mean_cutoff, color="red", linetype="dotted") +
        geom_vline(xintercept=floor(length(rolled_mean) / 2), color="black", 
                   linetype="dashed", size=1.5) +
        scale_shape_manual(values=c("Peak-Good"=24, "Peak-Artifact"=2, 
                                    "Valley-Good"=25, "Valley-Artifact"=6)) +
        scale_color_manual(values=c(Candidate="#e30724", "Too short"="grey", 
                                    Best="#32bf0b", "CloseToTSS"="#7af589",
                                    "Too far downstream"="#FFA500")) +
        labs(title=paste(tx_info$tx_name, label)) +
        theme(panel.grid.major = element_blank(),
              panel.grid.minor = element_blank(),
              panel.background = element_blank(),
              axis.line = element_line(colour = "black")) +
        annotate("text", x=Inf, y=Inf, label=annotation_text, hjust=1, vjust=1)
    
	transcript_name = gsub("transcript:", "", tx_info$tx_name)
	outpath = file.path(outdir, "diagnostics", transcript_name)
	dir.create(outpath, recursive=TRUE, showWarnings=FALSE)
    pdf(file.path(outpath, paste0(transcript_name, " for ", label, ".pdf")))
    print(gg_obj)
    dev.off()
    
    gg_obj
}

# Given the coverage over the region flanking the TSS of a gene, identify
# nucleosome peaks and the NDR region.
identify_ndr_verbose = function(coverage, tx_info, do_plot=FALSE, label="unknown", cutoff_level = 0.6, max_downstream = 200, outdir="output") {
    # Ocampo uses a window of 101. Shouldn't make much of a difference.
    rolled_mean = RcppRoll::roll_mean(coverage, n=100)
    # Cutoff used by Ocampo.
    mean_cutoff = mean(coverage) * cutoff_level 
    
    # If we identify zero or one peak, we can'T find an NDR. Return NA.
    peak_df = get_peak_df(rolled_mean, cutoff=mean_cutoff)
    if(!is(peak_df, "data.frame") || nrow(peak_df) == 1) {
        return(NA)
    }
    
    # If we can't identify valleys, skip the rest.
    valley_df = get_peak_df(-rolled_mean, negate=TRUE)
    if(!is(valley_df, "data.frame")) {
        return(NA)
    }

    ndr_df = get_ndr_df(peak_df, rolled_mean, max_downstream)
    if(!is(ndr_df, "data.frame")) {
        return(NA)
    }
    
    res = build_gene_info(peak_df, ndr_df, tx_info, rolled_mean)
    
    gg_obj=NULL
    if(do_plot &&!is.na(res)) {
        gg_obj=plot_gene_ndr(coverage, rolled_mean, peak_df, valley_df, ndr_df, mean_cutoff, res, , outdir=outdir)
    }

    return(list(Peaks=peak_df, Valleys=valley_df, GeneInfo=res, plot=gg_obj))
}

identify_ndr = function(coverage, tx_info, do_plot=FALSE, label="unknown", cutoff_level = 0.6, max_downstream = 200) {
    res = identify_ndr_verbose(coverage, tx_info, do_plot, label, cutoff_level, max_downstream)
    if(is.na(res)) {
        return(NA)
    } else {
        return(res$GeneInfo)
    }
}