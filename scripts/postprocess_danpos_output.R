# DANPOS sometimes produces dyad positions that are outside the bounds of
# the chromosomes. This script filters out these dyads.
# DANPOS also stores wild-type and treatment results in the same file.
# We will split those into their separate files.

library(dplyr)

output_folder = "output/danpos_input_auto/2019-10-09GMT"
all_files = Sys.glob(file.path(output_folder, "*_vs_*/*.positions.integrative.xls"))

chr_sizes = read.table("input/sacCer3.sizes", sep="\t")
colnames(chr_sizes) = c("chr", "size")
chr_sizes$chr = gsub("chr", "", chr_sizes$chr)

for(i in all_files) {
    # Extract the conditions from the filename.
    sample_names = c(treat = gsub("(.*)-(.*).positions.integrative.xls", "\\1", basename(i)),
                     control = gsub("(.*)-(.*).positions.integrative.xls", "\\2", basename(i)))

    # Remove out-of-bound entries.
    file_data = read.table(i, sep="\t", header=TRUE, stringsAsFactors=FALSE)
    file_data = left_join(file_data, chr_sizes)
    keep_indices = (file_data$end < file_data$size) & 
                   (file_data$start > 0) & (file_data$center > 0) &
                   !is.na(file_data$size)
    file_data = file_data[keep_indices,]
    
    # Split files into treatment/control pair.
    for(condition in names(sample_names)) {
        location_column = paste0(condition, "_smt_loca")
        condition_data = file_data[file_data[[location_column]] != "-",]
        condition_data$start = condition_data[[location_column]]
        condition_data$end = condition_data[[location_column]]
        out_table = file.path(output_folder, paste0(sample_names[condition], ".txt"))
        write.table(condition_data, file=out_table, sep="\t", col.names=TRUE, row.names=FALSE)
    }
}