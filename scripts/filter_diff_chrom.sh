#!/bin/bash
mkdir -p output/jobs
mkdir -p output/danpos_input_filter

for i in output/pipeline/alignment/*/*.sorted.dup.bam
do
    script=output/jobs/$sample.danpos_bam_filter.sh
    sample=`basename $i .sorted.dup.bam`

cat <<EOF > $script
#!/bin/bash
module load samtools
samtools view -h -b -f 2 $i > output/danpos_input_filter/$sample.bam
samtools index output/danpos_input_filter/$sample.bam
EOF

sbatch -D `pwd` --time 4:00:00 --mem 32G --cpus-per-task 1 --account def-amnou -o $script.stdout -e $script.stderr $script
done
