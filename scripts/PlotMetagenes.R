source("scripts/GenerateMetagenes.R")

# Make a plot combining all clones.
mg_df = list()
for(i in names(mg_list)) {
    mg_df[[i]] = mg_list[[i]]$add_metadata()
    mg_df[[i]]$SampleType = i
}
mg_all_df = do.call(rbind, mg_df)

nucleosome_size=169
n_nucleosome_on_one_side = FLANK_SIZE / nucleosome_size
nb_bin = max(mg_all_df$bin)

centered_pos = (mg_all_df$bin / ( nb_bin / 2)) - 1
pos_in_nucleosome = centered_pos * n_nucleosome_on_one_side
plus_one_adjusted = pos_in_nucleosome + 1

mg_all_df$Pos = plus_one_adjusted
mg_all_df$Clone = as.factor(mg_all_df$Clone)
pdf("output/NDR/metagene/Mnase-profiles.pdf", width=28, height=14)
ggplot(mg_all_df, aes(x=Pos, y=value, ymin=qinf, ymax=qsup)) +
    geom_line(aes(color=Clone, group=Clone)) +
    geom_ribbon(aes(fill=Clone, group=Clone), alpha=0.3) +
    geom_vline(xintercept=c(-3, -2, -1, 0, 2, 3, 4, 5), linetype="dashed", alpha=0.2) +
    geom_vline(xintercept=1, linetype="dashed", alpha=0.5) +
    facet_wrap(~SampleType, nrow=2)
dev.off()

for(i in 1:nrow(dyad_df)) {
    mg_subset = mg_all_df %>% filter(SampleType %in% c(dyad_df$Ref[i], dyad_df$Sample[i]))
    pdf(paste0("output/NDR/metagene/", dyad_df$Sample[i], " vs WT.pdf"), width=7, height=7)
    plot_obj = ggplot(mg_subset, aes(x=Pos, y=value, ymin=qinf, ymax=qsup)) +
        geom_line(aes(color=SampleType, group=Sample)) +
        geom_ribbon(aes(fill=SampleType, group=Sample), alpha=0.15) +
        geom_vline(xintercept=c(-3, -2, -1, 0, 2, 3, 4, 5), linetype="dashed", alpha=0.2) +
        geom_vline(xintercept=1, linetype="dashed", alpha=0.5)
    print(plot_obj)
    dev.off()
}


#mg = metagene2::metagene2$new(bam_files = design$BAM,
#                              regions=all_dyads["Ocampo_WT_30"], 
#                              paired_end=TRUE,
#                              normalization="RPM", 
#                              bin_count=300,
#                              cores=SnowParam(4),
#                              verbose=TRUE,
#                              design_metadata=design)
#mg$produce_metagene()
#
#pdf("Heatmap Ocampo NDR.pdf")
#metagene2_heatmap(mg, coverage_order(mg), scale_trans="log1p")
#dev.off()

bin_to_nt = function(bin, bin_size, zero_point) {
    bin = bin * bin_size - zero_point
}

plot_comparative_heatmap = function(mg_list, ctrl, treatment, region_order, 
                                    order_label, output_folder, draw_lines=FALSE) {
    dir.create(output_folder, recursive=TRUE, showWarnings=FALSE)                                    
                                    
    # Re-bin coverages at 300 bins, otherwise the heatmap becomes way too
    # heavy for ggplot.
    mg_list[[treatment]]$bin_coverages(bin_count=300)
    mg_list[[ctrl]]$bin_coverages(bin_count=300)
    
    # Get the heatmap data-frames and merge them.
    treat_df = metagene2:::combine_coverages(mg_list[[treatment]]$split_coverages_by_regions(), region_order, scale="row", smooth_sigma=NA)
    ctrl_df = metagene2:::combine_coverages(mg_list[[ctrl]]$split_coverages_by_regions(), region_order, scale="row", smooth_sigma=NA)
    heatmap_df = rbind(treat_df, ctrl_df)
    
    # Fix heatmap data-frame columns:
    # Fix bam names
    heatmap_df$bam_name = gsub(".sorted.dup", "", heatmap_df$bam_name)
    heatmap_df$bam_name = gsub("Ocampo_", "", heatmap_df$bam_name)
    
    # Refactor bam names so that WT is always on the left.
    factor_levels = unique(heatmap_df$bam_name)
    factor_WT = grep("WT", factor_levels, value=TRUE)
    factor_levels = c(factor_WT, setdiff(factor_levels, factor_WT))
    heatmap_df$bam_name = factor(heatmap_df$bam_name, levels=factor_levels) 
    
    # Build the graph's caption.
    caption = paste0("Dashed line represents the position of the +1 nucleosome;\n",
                     "dotted lines represent the expected positions of the +2, +3,",
                     "+4 and +5 nucleosomes.")
    
    # Redefine nt-based x-axis labels.
    bin_count = mg_list[[ctrl]]$get_params()$bin_count
    half_point = floor(bin_count/2)
    break_dist = (floor(half_point / 3))
    breaks = half_point+(-3:3)*break_dist
    region_width = unique(width(mg_list[[ctrl]]$get_regions()))[1]
    bin_size = region_width / bin_count
    breaks_labels = bin_to_nt(breaks, bin_size, floor(region_width/2))
    
    #scale_midpoint = log(max(heatmap_df$coverage)) / 2
    scale_midpoint = 0
    
    # Build the heatmap and save it to a pdf.
    pdf(paste0(output_folder, "/Heatmap for ", treatment, ".pdf"), width=14, height=14)
    gg_obj = ggplot(heatmap_df) +
        geom_raster(mapping=aes_string(x="bin", y="region", fill="coverage")) + 
        facet_grid(region_name~bam_name) +
        scale_y_continuous(trans = "reverse") +
        scale_fill_gradient(low="#FFFFFF", high="#18448c") +
        #scale_fill_gradient2(low="darkblue", high="darkred", mid="green", midpoint=scale_midpoint) +
        scale_x_continuous(breaks=breaks,
                           labels=breaks_labels) +
        labs(x="Distance from +1 nucleosome",  y="Regions",
             title=paste0("Comparative heatmap of Mnase-seq coverage for ", treatment),
             subtitle=paste0("Regions ordered by ", order_label)) +
        theme(panel.grid.major = element_blank(), 
              panel.grid.minor = element_blank(),
              panel.background = element_blank(),
              axis.line = element_line(colour = "black"),
              strip.background = element_blank(),
              panel.border = element_rect(fill=NA, colour = "black"))#,
#              axis.text.y=element_blank(),
#              axis.ticks.y=element_blank()) 
    if(draw_lines) {
        gg_obj = gg_obj +
            geom_vline(xintercept=half_point, linetype="dashed") +
            geom_vline(xintercept=half_point+(169/bin_size)*(0:4), linetype="dotted") +
            labs(caption=caption)
    }
    print(gg_obj)
    dev.off()
}

library(ComplexHeatmap)
plot_negative_heatmap = function(mg_list, ctrl, treatment, region_order, 
                                 order_label, output_folder) {
    dir.create(output_folder, recursive=TRUE, showWarnings=FALSE)                                   
                                 
    cond_mean_mat = function(mg) {
        split_mg = mg$split_coverages_by_regions()
        return((split_mg[[1]][["regions"]] + split_mg[[2]][["regions"]]) / 2)
    }
    
    # Plot negative heatmap.
    mean_ctrl = cond_mean_mat(mg_list[[ctrl]])
    mean_treat = cond_mean_mat(mg_list[[treatment]])
    #mean_neg = mean_ctrl - mean_treat
    mean_neg = log2((mean_treat + 1) / (mean_ctrl))[region_order,]  

    pdf(paste0(output_folder, "/Heatmap for ", treatment, ".pdf"), width=7, height=7)    
    print(Heatmap(mean_neg, cluster_rows=FALSE, cluster_columns=FALSE,
                  row_title=paste0("Regions, ", order_label),
                  column_title=paste0("Negative heatmap between ", treatment, " and WT\n"),
                  heatmap_legend_param=list(title="log2(Mutant/WT)")))
    dev.off()
}

plot_comparative_metagene = function(mg_list, treatments, design, output_folder, experimenter) {
    all_dfs = lapply(treatments, function(x) {
        ctrl = design$Ref[design$Label==x][1]
        res = rbind(mg_list[[x]]$add_metadata(), mg_list[[ctrl]]$add_metadata())
        res$Panel = x
        
        res
    })
    
    full_df = do.call(rbind, all_dfs)

    pdf(paste0(output_folder, "/Comparative metagene for ", experimenter, ".pdf"), width=7, height=7)
    wrap_nrow = ifelse(length(unique(full_df$Panel)) < 6, 2, 3)
    treat_subset = full_df[!grepl("WT", full_df$Label),]
    ctrl_subset = full_df[grepl("WT", full_df$Label),]
    gg_obj = ggplot() +
        geom_line(treat_subset, mapping=aes(x=bin * 5 - 750, y=value, color=Clone, group=Clone), size=0.5) +
        geom_ribbon(ctrl_subset, mapping=aes(x=bin * 5 - 750, ymax=value), ymin=0, color=NA, fill="grey", alpha=0.4) +
        facet_wrap(~Panel, nrow=wrap_nrow) +
        labs(x="Distance from +1 nucleosome") +
        theme_bw()    
    print(gg_obj)
    dev.off()
}

# Reset the design so we get the initial correct Ref column

plot_scaled_metagene = function(mg_list, ctrl, treatment, region_order, 
                                output_folder) {
    dir.create(output_folder, recursive=TRUE, showWarnings=FALSE)                                    
                                    
    # Re-bin coverages at 300 bins, otherwise the heatmap becomes way too
    # heavy for ggplot.
    mg_list[[treatment]]$bin_coverages(bin_count=300)
    mg_list[[ctrl]]$bin_coverages(bin_count=300)
    
    # Get the heatmap data-frames and merge them.
    treat_mat= scale_matrices(mg_list[[treatment]]$split_coverages_by_regions(), region_order, scale="row", smooth_sigma=NA)
    ctrl_mat = scale_matrices(mg_list[[ctrl]]$split_coverages_by_regions(), region_order, scale="row", smooth_sigma=NA)

    treat_df = metagene2:::calculate_matrices_ci(treat_mat, 0, 0.05, "bin")
    ctrl_df = metagene2:::calculate_matrices_ci(ctrl_mat, 0, 0.05, "bin")

    full_df = rbind(treat_df, ctrl_df)
    full_df$group = paste0(full_df$design, "_", full_df$region)
    clean_bam = gsub(".sorted.dup", "", gsub("Ocampo_", "", full_df$design))
    full_df$Strain = gsub("(.*)_(.*)", "\\1", clean_bam)
    full_df$Clone = gsub("(.*)_(.*)", "\\2", clean_bam)
    
    pdf(paste0(output_folder, "/Comparative scaled metagene for ", treatment, ".pdf"), width=7, height=7)
    gg_obj = ggplot() + 
        geom_line(data=full_df[!grepl("WT", full_df$Strain),],
                  mapping=aes(x=bin, y=value, group=Clone, color=Clone), size=1) +
        geom_ribbon(data=full_df[grepl("WT", full_df$Strain),],
                    mapping=aes(x=bin, ymax=value, group=Clone), ymin=0, color=NA, fill="grey", alpha=0.4) +
        theme_bw()
    print(gg_obj)
    dev.off()
}

scale_matrices = function(cov_matrices, region_order, scale=NA, smooth_sigma=NA) {
    res = list()
    for(bam in names(cov_matrices)) {
        res[[bam]] = list()
        for(region_name in names(cov_matrices[[bam]])) {
            input_matrix = cov_matrices[[bam]][[region_name]][region_order[[region_name]],]
            if(scale=="row") {
				zero_rows = apply(input_matrix, 1, max) == 0
                input_matrix = apply(input_matrix, 2, "/", apply(input_matrix, 1, max))
				input_matrix[zero_rows,] = 0
            }
            
            if(!is.na(smooth_sigma)) {
                input_matrix = as.matrix(spatstat::blur(spatstat::as.im(input_matrix), sigma=smooth_sigma))
            }
            res[[bam]][[region_name]] = input_matrix
        }
    }

    res
}

library(dplyr)
design = load_design_combined()
ndr_info_Cl2 = read.table("output/NDR/WT_30C_Cl2.txt", header=TRUE)
ndr_info_Cl3 = read.table("output/NDR/WT_30C_Cl3.txt", header=TRUE)
d_diff = ndr_info_Cl3$d_glm - ndr_info_Cl2$d_glm
d_mean = (ndr_info_Cl3$d_glm + ndr_info_Cl2$d_glm) / 2
av_d_diff = ifelse(abs(d_diff) <= 10, d_mean, NA)
#ndr_info = ndr_info[match(mg_list[[1]]$get_regions()$tx_name, ndr_info$tx_name),]
#av_d_diff = av_d_diff[match(mg_list[[1]]$get_regions()$tx_name, ndr_info$tx_name)]
for(experimenter in unique(design$Experimenter)) {
    output_folder = paste0("output/NDR/metagene/", experimenter)
    dir.create(output_folder, recursive=TRUE, showWarnings=FALSE)
    
    design_subset = design %>% filter(Experimenter == experimenter)
    unique_condition = unique(design_subset$Label)
    unique_treatment = setdiff(unique_condition, grep("WT", unique_condition, value=TRUE))
    
    #plot_comparative_metagene(mg_list, unique_treatment, design_subset,
    #                          output_folder, experimenter)
    
    for(treatment in unique_treatment) {
        ctrl = unique(design$Ref[design$Label==treatment])
        cov_order = coverage_order(mg_list[[ctrl]])
        d_order = list(regions=order(d_mean, decreasing=TRUE))
        
        plot_scaled_metagene(mg_list, ctrl, treatment, cov_order, output_folder)
        
        #plot_comparative_heatmap(mg_list, ctrl, treatment, cov_order,
        #                         "highest to lowest average coverage",
        #                         file.path(output_folder, "coverage"))
        #plot_comparative_heatmap(mg_list, ctrl, treatment, d_order,
        #                         "highest to lowest estimated spacing",
        #                         file.path(output_folder, "spacing"))
        #                         
        #plot_negative_heatmap(mg_list, ctrl, treatment, cov_order[[1]],
        #                      "highest to lowest average coverage in WT",
        #                      file.path(output_folder, "negative-coverage"))
        #plot_negative_heatmap(mg_list, ctrl, treatment, d_order[[1]],
        #                      "highest to lowest estimated spacing in WT",
        #                      file.path(output_folder, "negative-spacing"))                              
        
    }
}

ndr_info_1 = read.table("output/NDR/WT_30C_Cl2.txt", header=TRUE)
ndr_info_2 = read.table("output/NDR/WT_30C_Cl3.txt", header=TRUE)


