#!/bin/bash

for i in  */*.pair?.fastq.gz; 
do 
    if [ ! -e $i.old ]
    then
        zcat $i | sed -E "s/^((@|\+)SRR[^.]+\.[^.]+)\.(1|2)/\1/" | gzip -c > $i.fixed; 
        mv $i $i.old; 
        mv $i.fixed $i; 
        echo $i OK
    else
        echo $i skip
    fi
done;

