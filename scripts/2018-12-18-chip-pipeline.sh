#!/bin/bash
# Exit immediately on error
set -eu -o pipefail

#-------------------------------------------------------------------------------
# ChipSeq SlurmScheduler Job Submission Bash script
# Version: 3.1.2-beta
# Created on: 2018-12-18T20:07:17
# Steps:
#   picard_sam_to_fastq: 0 job... skipping
#   trimmomatic: 14 jobs
#   merge_trimmomatic_stats: 1 job
#   bwa_mem_picard_sort_sam: 15 jobs
#   samtools_view_filter: 15 jobs
#   picard_merge_sam_files: 14 jobs
#   picard_mark_duplicates: 15 jobs
#   TOTAL: 74 jobs
#-------------------------------------------------------------------------------

OUTPUT_DIR=/project/6007406/efournie/Mnase/output/pipeline
JOB_OUTPUT_DIR=$OUTPUT_DIR/job_output
TIMESTAMP=`date +%FT%H.%M.%S`
JOB_LIST=$JOB_OUTPUT_DIR/ChipSeq_job_list_$TIMESTAMP
export CONFIG_FILES="/cvmfs/soft.mugqic/CentOS6/software/genpipes/genpipes-3.1.2/pipelines/chipseq/chipseq.base.ini,/cvmfs/soft.mugqic/CentOS6/software/genpipes/genpipes-3.1.2/pipelines/chipseq/chipseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/Saccharomyces_cerevisiae.R64-1-1.ini"
mkdir -p $OUTPUT_DIR
cd $OUTPUT_DIR


#-------------------------------------------------------------------------------
# STEP: trimmomatic
#-------------------------------------------------------------------------------
STEP=trimmomatic
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: trimmomatic_1_JOB_ID: trimmomatic.HI.4594.004.Index_22.Mnase_dspt2_YAR669_Cl2
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.HI.4594.004.Index_22.Mnase_dspt2_YAR669_Cl2
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.HI.4594.004.Index_22.Mnase_dspt2_YAR669_Cl2.edaf41498ae783c8a90cc552ff963b89.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.HI.4594.004.Index_22.Mnase_dspt2_YAR669_Cl2.edaf41498ae783c8a90cc552ff963b89.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/trimmomatic/0.36 && \
mkdir -p trim/dspt2_30C_Cl2 && \
`cat > trim/dspt2_30C_Cl2/HI.4594.004.Index_22.Mnase_dspt2_YAR669_Cl2.trim.adapters.fa << END
>Prefix/1
ACACTCTTTCCCTACACGACGCTCTTCCGATCT
>Prefix/2
GTGACTGGAGTTCAGACGTGTGCTCTTCCGATCT
END
` && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR PE \
  -threads 1 \
  -phred33 \
  /project/6007406/efournie/Mnase/raw/HI.4594.004.Index_22.Mnase_dspt2_YAR669_Cl2_R1.fastq.gz \
  /project/6007406/efournie/Mnase/raw/HI.4594.004.Index_22.Mnase_dspt2_YAR669_Cl2_R2.fastq.gz \
  trim/dspt2_30C_Cl2/HI.4594.004.Index_22.Mnase_dspt2_YAR669_Cl2.trim.pair1.fastq.gz \
  trim/dspt2_30C_Cl2/HI.4594.004.Index_22.Mnase_dspt2_YAR669_Cl2.trim.single1.fastq.gz \
  trim/dspt2_30C_Cl2/HI.4594.004.Index_22.Mnase_dspt2_YAR669_Cl2.trim.pair2.fastq.gz \
  trim/dspt2_30C_Cl2/HI.4594.004.Index_22.Mnase_dspt2_YAR669_Cl2.trim.single2.fastq.gz \
  ILLUMINACLIP:trim/dspt2_30C_Cl2/HI.4594.004.Index_22.Mnase_dspt2_YAR669_Cl2.trim.adapters.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:50 \
  2> trim/dspt2_30C_Cl2/HI.4594.004.Index_22.Mnase_dspt2_YAR669_Cl2.trim.log
trimmomatic.HI.4594.004.Index_22.Mnase_dspt2_YAR669_Cl2.edaf41498ae783c8a90cc552ff963b89.mugqic.done
)
trimmomatic_1_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 | grep "[0-9]" | cut -d\  -f4)
echo "$trimmomatic_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: trimmomatic_2_JOB_ID: trimmomatic.HI.4594.004.Index_25.Mnase_dspt2_YAR669_Cl3
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.HI.4594.004.Index_25.Mnase_dspt2_YAR669_Cl3
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.HI.4594.004.Index_25.Mnase_dspt2_YAR669_Cl3.6597368e54c3d8b19b5ed2f8cdf09c42.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.HI.4594.004.Index_25.Mnase_dspt2_YAR669_Cl3.6597368e54c3d8b19b5ed2f8cdf09c42.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/trimmomatic/0.36 && \
mkdir -p trim/dspt2_30C_Cl3 && \
`cat > trim/dspt2_30C_Cl3/HI.4594.004.Index_25.Mnase_dspt2_YAR669_Cl3.trim.adapters.fa << END
>Prefix/1
ACACTCTTTCCCTACACGACGCTCTTCCGATCT
>Prefix/2
GTGACTGGAGTTCAGACGTGTGCTCTTCCGATCT
END
` && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR PE \
  -threads 1 \
  -phred33 \
  /project/6007406/efournie/Mnase/raw/HI.4594.004.Index_25.Mnase_dspt2_YAR669_Cl3_R1.fastq.gz \
  /project/6007406/efournie/Mnase/raw/HI.4594.004.Index_25.Mnase_dspt2_YAR669_Cl3_R2.fastq.gz \
  trim/dspt2_30C_Cl3/HI.4594.004.Index_25.Mnase_dspt2_YAR669_Cl3.trim.pair1.fastq.gz \
  trim/dspt2_30C_Cl3/HI.4594.004.Index_25.Mnase_dspt2_YAR669_Cl3.trim.single1.fastq.gz \
  trim/dspt2_30C_Cl3/HI.4594.004.Index_25.Mnase_dspt2_YAR669_Cl3.trim.pair2.fastq.gz \
  trim/dspt2_30C_Cl3/HI.4594.004.Index_25.Mnase_dspt2_YAR669_Cl3.trim.single2.fastq.gz \
  ILLUMINACLIP:trim/dspt2_30C_Cl3/HI.4594.004.Index_25.Mnase_dspt2_YAR669_Cl3.trim.adapters.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:50 \
  2> trim/dspt2_30C_Cl3/HI.4594.004.Index_25.Mnase_dspt2_YAR669_Cl3.trim.log
trimmomatic.HI.4594.004.Index_25.Mnase_dspt2_YAR669_Cl3.6597368e54c3d8b19b5ed2f8cdf09c42.mugqic.done
)
trimmomatic_2_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 | grep "[0-9]" | cut -d\  -f4)
echo "$trimmomatic_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: trimmomatic_3_JOB_ID: trimmomatic.HI.4594.004.Index_27.Mnase_spt2m_YAR673_Cl2
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.HI.4594.004.Index_27.Mnase_spt2m_YAR673_Cl2
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.HI.4594.004.Index_27.Mnase_spt2m_YAR673_Cl2.517d9341d879ffc8f078006d2785f79a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.HI.4594.004.Index_27.Mnase_spt2m_YAR673_Cl2.517d9341d879ffc8f078006d2785f79a.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/trimmomatic/0.36 && \
mkdir -p trim/spt2m_30C_Cl2 && \
`cat > trim/spt2m_30C_Cl2/HI.4594.004.Index_27.Mnase_spt2m_YAR673_Cl2.trim.adapters.fa << END
>Prefix/1
ACACTCTTTCCCTACACGACGCTCTTCCGATCT
>Prefix/2
GTGACTGGAGTTCAGACGTGTGCTCTTCCGATCT
END
` && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR PE \
  -threads 1 \
  -phred33 \
  /project/6007406/efournie/Mnase/raw/HI.4594.004.Index_27.Mnase_spt2m_YAR673_Cl2_R1.fastq.gz \
  /project/6007406/efournie/Mnase/raw/HI.4594.004.Index_27.Mnase_spt2m_YAR673_Cl2_R2.fastq.gz \
  trim/spt2m_30C_Cl2/HI.4594.004.Index_27.Mnase_spt2m_YAR673_Cl2.trim.pair1.fastq.gz \
  trim/spt2m_30C_Cl2/HI.4594.004.Index_27.Mnase_spt2m_YAR673_Cl2.trim.single1.fastq.gz \
  trim/spt2m_30C_Cl2/HI.4594.004.Index_27.Mnase_spt2m_YAR673_Cl2.trim.pair2.fastq.gz \
  trim/spt2m_30C_Cl2/HI.4594.004.Index_27.Mnase_spt2m_YAR673_Cl2.trim.single2.fastq.gz \
  ILLUMINACLIP:trim/spt2m_30C_Cl2/HI.4594.004.Index_27.Mnase_spt2m_YAR673_Cl2.trim.adapters.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:50 \
  2> trim/spt2m_30C_Cl2/HI.4594.004.Index_27.Mnase_spt2m_YAR673_Cl2.trim.log
trimmomatic.HI.4594.004.Index_27.Mnase_spt2m_YAR673_Cl2.517d9341d879ffc8f078006d2785f79a.mugqic.done
)
trimmomatic_3_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 | grep "[0-9]" | cut -d\  -f4)
echo "$trimmomatic_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: trimmomatic_4_JOB_ID: trimmomatic.HI.4594.004.Index_2.Mnase_spt2m_YAR673_Cl3
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.HI.4594.004.Index_2.Mnase_spt2m_YAR673_Cl3
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.HI.4594.004.Index_2.Mnase_spt2m_YAR673_Cl3.a0973ed2706d2a259f3b6a9f1e42e559.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.HI.4594.004.Index_2.Mnase_spt2m_YAR673_Cl3.a0973ed2706d2a259f3b6a9f1e42e559.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/trimmomatic/0.36 && \
mkdir -p trim/spt2m_30C_Cl3 && \
`cat > trim/spt2m_30C_Cl3/HI.4594.004.Index_2.Mnase_spt2m_YAR673_Cl3.trim.adapters.fa << END
>Prefix/1
ACACTCTTTCCCTACACGACGCTCTTCCGATCT
>Prefix/2
GTGACTGGAGTTCAGACGTGTGCTCTTCCGATCT
END
` && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR PE \
  -threads 1 \
  -phred33 \
  /project/6007406/efournie/Mnase/raw/HI.4594.004.Index_2.Mnase_spt2m_YAR673_Cl3_R1.fastq.gz \
  /project/6007406/efournie/Mnase/raw/HI.4594.004.Index_2.Mnase_spt2m_YAR673_Cl3_R2.fastq.gz \
  trim/spt2m_30C_Cl3/HI.4594.004.Index_2.Mnase_spt2m_YAR673_Cl3.trim.pair1.fastq.gz \
  trim/spt2m_30C_Cl3/HI.4594.004.Index_2.Mnase_spt2m_YAR673_Cl3.trim.single1.fastq.gz \
  trim/spt2m_30C_Cl3/HI.4594.004.Index_2.Mnase_spt2m_YAR673_Cl3.trim.pair2.fastq.gz \
  trim/spt2m_30C_Cl3/HI.4594.004.Index_2.Mnase_spt2m_YAR673_Cl3.trim.single2.fastq.gz \
  ILLUMINACLIP:trim/spt2m_30C_Cl3/HI.4594.004.Index_2.Mnase_spt2m_YAR673_Cl3.trim.adapters.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:50 \
  2> trim/spt2m_30C_Cl3/HI.4594.004.Index_2.Mnase_spt2m_YAR673_Cl3.trim.log
trimmomatic.HI.4594.004.Index_2.Mnase_spt2m_YAR673_Cl3.a0973ed2706d2a259f3b6a9f1e42e559.mugqic.done
)
trimmomatic_4_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 | grep "[0-9]" | cut -d\  -f4)
echo "$trimmomatic_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: trimmomatic_5_JOB_ID: trimmomatic.HI.4594.004.Index_21.Mnase_WT_YAR670_Cl2
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.HI.4594.004.Index_21.Mnase_WT_YAR670_Cl2
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.HI.4594.004.Index_21.Mnase_WT_YAR670_Cl2.dd14918a2c6ad6074b7b85aa39014838.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.HI.4594.004.Index_21.Mnase_WT_YAR670_Cl2.dd14918a2c6ad6074b7b85aa39014838.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/trimmomatic/0.36 && \
mkdir -p trim/WT_30C_Cl2 && \
`cat > trim/WT_30C_Cl2/HI.4594.004.Index_21.Mnase_WT_YAR670_Cl2.trim.adapters.fa << END
>Prefix/1
ACACTCTTTCCCTACACGACGCTCTTCCGATCT
>Prefix/2
GTGACTGGAGTTCAGACGTGTGCTCTTCCGATCT
END
` && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR PE \
  -threads 1 \
  -phred33 \
  /project/6007406/efournie/Mnase/raw/HI.4594.004.Index_21.Mnase_WT_YAR670_Cl2_R1.fastq.gz \
  /project/6007406/efournie/Mnase/raw/HI.4594.004.Index_21.Mnase_WT_YAR670_Cl2_R2.fastq.gz \
  trim/WT_30C_Cl2/HI.4594.004.Index_21.Mnase_WT_YAR670_Cl2.trim.pair1.fastq.gz \
  trim/WT_30C_Cl2/HI.4594.004.Index_21.Mnase_WT_YAR670_Cl2.trim.single1.fastq.gz \
  trim/WT_30C_Cl2/HI.4594.004.Index_21.Mnase_WT_YAR670_Cl2.trim.pair2.fastq.gz \
  trim/WT_30C_Cl2/HI.4594.004.Index_21.Mnase_WT_YAR670_Cl2.trim.single2.fastq.gz \
  ILLUMINACLIP:trim/WT_30C_Cl2/HI.4594.004.Index_21.Mnase_WT_YAR670_Cl2.trim.adapters.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:50 \
  2> trim/WT_30C_Cl2/HI.4594.004.Index_21.Mnase_WT_YAR670_Cl2.trim.log
trimmomatic.HI.4594.004.Index_21.Mnase_WT_YAR670_Cl2.dd14918a2c6ad6074b7b85aa39014838.mugqic.done
)
trimmomatic_5_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 | grep "[0-9]" | cut -d\  -f4)
echo "$trimmomatic_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: trimmomatic_6_JOB_ID: trimmomatic.HI.4594.004.Index_23.Mnase_WT_YAR670_Cl3
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.HI.4594.004.Index_23.Mnase_WT_YAR670_Cl3
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.HI.4594.004.Index_23.Mnase_WT_YAR670_Cl3.0bd65707e145f15ad50e2064a96d3370.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.HI.4594.004.Index_23.Mnase_WT_YAR670_Cl3.0bd65707e145f15ad50e2064a96d3370.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/trimmomatic/0.36 && \
mkdir -p trim/WT_30C_Cl3 && \
`cat > trim/WT_30C_Cl3/HI.4594.004.Index_23.Mnase_WT_YAR670_Cl3.trim.adapters.fa << END
>Prefix/1
ACACTCTTTCCCTACACGACGCTCTTCCGATCT
>Prefix/2
GTGACTGGAGTTCAGACGTGTGCTCTTCCGATCT
END
` && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR PE \
  -threads 1 \
  -phred33 \
  /project/6007406/efournie/Mnase/raw/HI.4594.004.Index_23.Mnase_WT_YAR670_Cl3_R1.fastq.gz \
  /project/6007406/efournie/Mnase/raw/HI.4594.004.Index_23.Mnase_WT_YAR670_Cl3_R2.fastq.gz \
  trim/WT_30C_Cl3/HI.4594.004.Index_23.Mnase_WT_YAR670_Cl3.trim.pair1.fastq.gz \
  trim/WT_30C_Cl3/HI.4594.004.Index_23.Mnase_WT_YAR670_Cl3.trim.single1.fastq.gz \
  trim/WT_30C_Cl3/HI.4594.004.Index_23.Mnase_WT_YAR670_Cl3.trim.pair2.fastq.gz \
  trim/WT_30C_Cl3/HI.4594.004.Index_23.Mnase_WT_YAR670_Cl3.trim.single2.fastq.gz \
  ILLUMINACLIP:trim/WT_30C_Cl3/HI.4594.004.Index_23.Mnase_WT_YAR670_Cl3.trim.adapters.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:50 \
  2> trim/WT_30C_Cl3/HI.4594.004.Index_23.Mnase_WT_YAR670_Cl3.trim.log
trimmomatic.HI.4594.004.Index_23.Mnase_WT_YAR670_Cl3.0bd65707e145f15ad50e2064a96d3370.mugqic.done
)
trimmomatic_6_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 | grep "[0-9]" | cut -d\  -f4)
echo "$trimmomatic_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: trimmomatic_7_JOB_ID: trimmomatic.HI.4594.004.Index_15.Mnase_Exp3_CKIIts_37C2h_Cl1
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.HI.4594.004.Index_15.Mnase_Exp3_CKIIts_37C2h_Cl1
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.HI.4594.004.Index_15.Mnase_Exp3_CKIIts_37C2h_Cl1.8ca8f07d6099e864461892e8de61cd1a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.HI.4594.004.Index_15.Mnase_Exp3_CKIIts_37C2h_Cl1.8ca8f07d6099e864461892e8de61cd1a.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/trimmomatic/0.36 && \
mkdir -p trim/CKIIts_37C_Cl1 && \
`cat > trim/CKIIts_37C_Cl1/HI.4594.004.Index_15.Mnase_Exp3_CKIIts_37C2h_Cl1.trim.adapters.fa << END
>Prefix/1
ACACTCTTTCCCTACACGACGCTCTTCCGATCT
>Prefix/2
GTGACTGGAGTTCAGACGTGTGCTCTTCCGATCT
END
` && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR PE \
  -threads 1 \
  -phred33 \
  /project/6007406/efournie/Mnase/raw/HI.4594.004.Index_15.Mnase_Exp3_CKIIts_37C2h_Cl1_R1.fastq.gz \
  /project/6007406/efournie/Mnase/raw/HI.4594.004.Index_15.Mnase_Exp3_CKIIts_37C2h_Cl1_R2.fastq.gz \
  trim/CKIIts_37C_Cl1/HI.4594.004.Index_15.Mnase_Exp3_CKIIts_37C2h_Cl1.trim.pair1.fastq.gz \
  trim/CKIIts_37C_Cl1/HI.4594.004.Index_15.Mnase_Exp3_CKIIts_37C2h_Cl1.trim.single1.fastq.gz \
  trim/CKIIts_37C_Cl1/HI.4594.004.Index_15.Mnase_Exp3_CKIIts_37C2h_Cl1.trim.pair2.fastq.gz \
  trim/CKIIts_37C_Cl1/HI.4594.004.Index_15.Mnase_Exp3_CKIIts_37C2h_Cl1.trim.single2.fastq.gz \
  ILLUMINACLIP:trim/CKIIts_37C_Cl1/HI.4594.004.Index_15.Mnase_Exp3_CKIIts_37C2h_Cl1.trim.adapters.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:50 \
  2> trim/CKIIts_37C_Cl1/HI.4594.004.Index_15.Mnase_Exp3_CKIIts_37C2h_Cl1.trim.log
trimmomatic.HI.4594.004.Index_15.Mnase_Exp3_CKIIts_37C2h_Cl1.8ca8f07d6099e864461892e8de61cd1a.mugqic.done
)
trimmomatic_7_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 | grep "[0-9]" | cut -d\  -f4)
echo "$trimmomatic_7_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: trimmomatic_8_JOB_ID: trimmomatic.HI.4594.004.Index_7.Mnase_Exp3_CKIIts_37C2h_Cl2
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.HI.4594.004.Index_7.Mnase_Exp3_CKIIts_37C2h_Cl2
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.HI.4594.004.Index_7.Mnase_Exp3_CKIIts_37C2h_Cl2.713ac3552cb54a95b77450e288405e88.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.HI.4594.004.Index_7.Mnase_Exp3_CKIIts_37C2h_Cl2.713ac3552cb54a95b77450e288405e88.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/trimmomatic/0.36 && \
mkdir -p trim/CKIIts_37C_Cl2 && \
`cat > trim/CKIIts_37C_Cl2/HI.4594.004.Index_7.Mnase_Exp3_CKIIts_37C2h_Cl2.trim.adapters.fa << END
>Prefix/1
ACACTCTTTCCCTACACGACGCTCTTCCGATCT
>Prefix/2
GTGACTGGAGTTCAGACGTGTGCTCTTCCGATCT
END
` && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR PE \
  -threads 1 \
  -phred33 \
  /project/6007406/efournie/Mnase/raw/HI.4594.004.Index_7.Mnase_Exp3_CKIIts_37C2h_Cl2_R1.fastq.gz \
  /project/6007406/efournie/Mnase/raw/HI.4594.004.Index_7.Mnase_Exp3_CKIIts_37C2h_Cl2_R2.fastq.gz \
  trim/CKIIts_37C_Cl2/HI.4594.004.Index_7.Mnase_Exp3_CKIIts_37C2h_Cl2.trim.pair1.fastq.gz \
  trim/CKIIts_37C_Cl2/HI.4594.004.Index_7.Mnase_Exp3_CKIIts_37C2h_Cl2.trim.single1.fastq.gz \
  trim/CKIIts_37C_Cl2/HI.4594.004.Index_7.Mnase_Exp3_CKIIts_37C2h_Cl2.trim.pair2.fastq.gz \
  trim/CKIIts_37C_Cl2/HI.4594.004.Index_7.Mnase_Exp3_CKIIts_37C2h_Cl2.trim.single2.fastq.gz \
  ILLUMINACLIP:trim/CKIIts_37C_Cl2/HI.4594.004.Index_7.Mnase_Exp3_CKIIts_37C2h_Cl2.trim.adapters.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:50 \
  2> trim/CKIIts_37C_Cl2/HI.4594.004.Index_7.Mnase_Exp3_CKIIts_37C2h_Cl2.trim.log
trimmomatic.HI.4594.004.Index_7.Mnase_Exp3_CKIIts_37C2h_Cl2.713ac3552cb54a95b77450e288405e88.mugqic.done
)
trimmomatic_8_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 | grep "[0-9]" | cut -d\  -f4)
echo "$trimmomatic_8_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: trimmomatic_9_JOB_ID: trimmomatic.HI.4594.004.Index_13.Mnase_Exp3_WT37C2h_Cl1
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.HI.4594.004.Index_13.Mnase_Exp3_WT37C2h_Cl1
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.HI.4594.004.Index_13.Mnase_Exp3_WT37C2h_Cl1.3f4ff25bad3863f22019c08daf6fcf6b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.HI.4594.004.Index_13.Mnase_Exp3_WT37C2h_Cl1.3f4ff25bad3863f22019c08daf6fcf6b.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/trimmomatic/0.36 && \
mkdir -p trim/WT_37C_Cl1 && \
`cat > trim/WT_37C_Cl1/HI.4594.004.Index_13.Mnase_Exp3_WT37C2h_Cl1.trim.adapters.fa << END
>Prefix/1
ACACTCTTTCCCTACACGACGCTCTTCCGATCT
>Prefix/2
GTGACTGGAGTTCAGACGTGTGCTCTTCCGATCT
END
` && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR PE \
  -threads 1 \
  -phred33 \
  /project/6007406/efournie/Mnase/raw/HI.4594.004.Index_13.Mnase_Exp3_WT37C2h_Cl1_R1.fastq.gz \
  /project/6007406/efournie/Mnase/raw/HI.4594.004.Index_13.Mnase_Exp3_WT37C2h_Cl1_R2.fastq.gz \
  trim/WT_37C_Cl1/HI.4594.004.Index_13.Mnase_Exp3_WT37C2h_Cl1.trim.pair1.fastq.gz \
  trim/WT_37C_Cl1/HI.4594.004.Index_13.Mnase_Exp3_WT37C2h_Cl1.trim.single1.fastq.gz \
  trim/WT_37C_Cl1/HI.4594.004.Index_13.Mnase_Exp3_WT37C2h_Cl1.trim.pair2.fastq.gz \
  trim/WT_37C_Cl1/HI.4594.004.Index_13.Mnase_Exp3_WT37C2h_Cl1.trim.single2.fastq.gz \
  ILLUMINACLIP:trim/WT_37C_Cl1/HI.4594.004.Index_13.Mnase_Exp3_WT37C2h_Cl1.trim.adapters.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:50 \
  2> trim/WT_37C_Cl1/HI.4594.004.Index_13.Mnase_Exp3_WT37C2h_Cl1.trim.log
trimmomatic.HI.4594.004.Index_13.Mnase_Exp3_WT37C2h_Cl1.3f4ff25bad3863f22019c08daf6fcf6b.mugqic.done
)
trimmomatic_9_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 | grep "[0-9]" | cut -d\  -f4)
echo "$trimmomatic_9_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: trimmomatic_10_JOB_ID: trimmomatic.HI.4594.004.Index_6.Mnase_Exp3_WT37C2h_Cl2
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.HI.4594.004.Index_6.Mnase_Exp3_WT37C2h_Cl2
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.HI.4594.004.Index_6.Mnase_Exp3_WT37C2h_Cl2.be167e61bd181ac9e89fe18b7c7c4ac6.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.HI.4594.004.Index_6.Mnase_Exp3_WT37C2h_Cl2.be167e61bd181ac9e89fe18b7c7c4ac6.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/trimmomatic/0.36 && \
mkdir -p trim/WT_37C_Cl2 && \
`cat > trim/WT_37C_Cl2/HI.4594.004.Index_6.Mnase_Exp3_WT37C2h_Cl2.trim.adapters.fa << END
>Prefix/1
ACACTCTTTCCCTACACGACGCTCTTCCGATCT
>Prefix/2
GTGACTGGAGTTCAGACGTGTGCTCTTCCGATCT
END
` && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR PE \
  -threads 1 \
  -phred33 \
  /project/6007406/efournie/Mnase/raw/HI.4594.004.Index_6.Mnase_Exp3_WT37C2h_Cl2_R1.fastq.gz \
  /project/6007406/efournie/Mnase/raw/HI.4594.004.Index_6.Mnase_Exp3_WT37C2h_Cl2_R2.fastq.gz \
  trim/WT_37C_Cl2/HI.4594.004.Index_6.Mnase_Exp3_WT37C2h_Cl2.trim.pair1.fastq.gz \
  trim/WT_37C_Cl2/HI.4594.004.Index_6.Mnase_Exp3_WT37C2h_Cl2.trim.single1.fastq.gz \
  trim/WT_37C_Cl2/HI.4594.004.Index_6.Mnase_Exp3_WT37C2h_Cl2.trim.pair2.fastq.gz \
  trim/WT_37C_Cl2/HI.4594.004.Index_6.Mnase_Exp3_WT37C2h_Cl2.trim.single2.fastq.gz \
  ILLUMINACLIP:trim/WT_37C_Cl2/HI.4594.004.Index_6.Mnase_Exp3_WT37C2h_Cl2.trim.adapters.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:50 \
  2> trim/WT_37C_Cl2/HI.4594.004.Index_6.Mnase_Exp3_WT37C2h_Cl2.trim.log
trimmomatic.HI.4594.004.Index_6.Mnase_Exp3_WT37C2h_Cl2.be167e61bd181ac9e89fe18b7c7c4ac6.mugqic.done
)
trimmomatic_10_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 | grep "[0-9]" | cut -d\  -f4)
echo "$trimmomatic_10_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: trimmomatic_11_JOB_ID: trimmomatic.HI.4594.004.Index_11.Mnase_spt6ts_YAR844_39C1h_Cl1
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.HI.4594.004.Index_11.Mnase_spt6ts_YAR844_39C1h_Cl1
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.HI.4594.004.Index_11.Mnase_spt6ts_YAR844_39C1h_Cl1.955f6c6a2c271f1798f7b8e2c2babc82.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.HI.4594.004.Index_11.Mnase_spt6ts_YAR844_39C1h_Cl1.955f6c6a2c271f1798f7b8e2c2babc82.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/trimmomatic/0.36 && \
mkdir -p trim/spt6ts_39C_Cl1 && \
`cat > trim/spt6ts_39C_Cl1/HI.4594.004.Index_11.Mnase_spt6ts_YAR844_39C1h_Cl1.trim.adapters.fa << END
>Prefix/1
ACACTCTTTCCCTACACGACGCTCTTCCGATCT
>Prefix/2
GTGACTGGAGTTCAGACGTGTGCTCTTCCGATCT
END
` && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR PE \
  -threads 1 \
  -phred33 \
  /project/6007406/efournie/Mnase/raw/HI.4594.004.Index_11.Mnase_spt6ts_YAR844_39C1h_Cl1_R1.fastq.gz \
  /project/6007406/efournie/Mnase/raw/HI.4594.004.Index_11.Mnase_spt6ts_YAR844_39C1h_Cl1_R2.fastq.gz \
  trim/spt6ts_39C_Cl1/HI.4594.004.Index_11.Mnase_spt6ts_YAR844_39C1h_Cl1.trim.pair1.fastq.gz \
  trim/spt6ts_39C_Cl1/HI.4594.004.Index_11.Mnase_spt6ts_YAR844_39C1h_Cl1.trim.single1.fastq.gz \
  trim/spt6ts_39C_Cl1/HI.4594.004.Index_11.Mnase_spt6ts_YAR844_39C1h_Cl1.trim.pair2.fastq.gz \
  trim/spt6ts_39C_Cl1/HI.4594.004.Index_11.Mnase_spt6ts_YAR844_39C1h_Cl1.trim.single2.fastq.gz \
  ILLUMINACLIP:trim/spt6ts_39C_Cl1/HI.4594.004.Index_11.Mnase_spt6ts_YAR844_39C1h_Cl1.trim.adapters.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:50 \
  2> trim/spt6ts_39C_Cl1/HI.4594.004.Index_11.Mnase_spt6ts_YAR844_39C1h_Cl1.trim.log
trimmomatic.HI.4594.004.Index_11.Mnase_spt6ts_YAR844_39C1h_Cl1.955f6c6a2c271f1798f7b8e2c2babc82.mugqic.done
)
trimmomatic_11_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 | grep "[0-9]" | cut -d\  -f4)
echo "$trimmomatic_11_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: trimmomatic_12_JOB_ID: trimmomatic.HI.4594.004.Index_20.Mnase_spt6ts_YAR844_39C1h_Cl2
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.HI.4594.004.Index_20.Mnase_spt6ts_YAR844_39C1h_Cl2
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.HI.4594.004.Index_20.Mnase_spt6ts_YAR844_39C1h_Cl2.e79fa40501a5be1474ae8dcb133abc2d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.HI.4594.004.Index_20.Mnase_spt6ts_YAR844_39C1h_Cl2.e79fa40501a5be1474ae8dcb133abc2d.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/trimmomatic/0.36 && \
mkdir -p trim/spt6ts_39C_Cl2 && \
`cat > trim/spt6ts_39C_Cl2/HI.4594.004.Index_20.Mnase_spt6ts_YAR844_39C1h_Cl2.trim.adapters.fa << END
>Prefix/1
ACACTCTTTCCCTACACGACGCTCTTCCGATCT
>Prefix/2
GTGACTGGAGTTCAGACGTGTGCTCTTCCGATCT
END
` && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR PE \
  -threads 1 \
  -phred33 \
  /project/6007406/efournie/Mnase/raw/HI.4594.004.Index_20.Mnase_spt6ts_YAR844_39C1h_Cl2_R1.fastq.gz \
  /project/6007406/efournie/Mnase/raw/HI.4594.004.Index_20.Mnase_spt6ts_YAR844_39C1h_Cl2_R2.fastq.gz \
  trim/spt6ts_39C_Cl2/HI.4594.004.Index_20.Mnase_spt6ts_YAR844_39C1h_Cl2.trim.pair1.fastq.gz \
  trim/spt6ts_39C_Cl2/HI.4594.004.Index_20.Mnase_spt6ts_YAR844_39C1h_Cl2.trim.single1.fastq.gz \
  trim/spt6ts_39C_Cl2/HI.4594.004.Index_20.Mnase_spt6ts_YAR844_39C1h_Cl2.trim.pair2.fastq.gz \
  trim/spt6ts_39C_Cl2/HI.4594.004.Index_20.Mnase_spt6ts_YAR844_39C1h_Cl2.trim.single2.fastq.gz \
  ILLUMINACLIP:trim/spt6ts_39C_Cl2/HI.4594.004.Index_20.Mnase_spt6ts_YAR844_39C1h_Cl2.trim.adapters.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:50 \
  2> trim/spt6ts_39C_Cl2/HI.4594.004.Index_20.Mnase_spt6ts_YAR844_39C1h_Cl2.trim.log
trimmomatic.HI.4594.004.Index_20.Mnase_spt6ts_YAR844_39C1h_Cl2.e79fa40501a5be1474ae8dcb133abc2d.mugqic.done
)
trimmomatic_12_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 | grep "[0-9]" | cut -d\  -f4)
echo "$trimmomatic_12_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: trimmomatic_13_JOB_ID: trimmomatic.HI.4594.004.Index_8.Mnase_WT_YAR845_39C1h_Cl1
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.HI.4594.004.Index_8.Mnase_WT_YAR845_39C1h_Cl1
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.HI.4594.004.Index_8.Mnase_WT_YAR845_39C1h_Cl1.0205187a8795c48150bcd76d669d2aa8.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.HI.4594.004.Index_8.Mnase_WT_YAR845_39C1h_Cl1.0205187a8795c48150bcd76d669d2aa8.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/trimmomatic/0.36 && \
mkdir -p trim/WT_39C_Cl1 && \
`cat > trim/WT_39C_Cl1/HI.4594.004.Index_8.Mnase_WT_YAR845_39C1h_Cl1.trim.adapters.fa << END
>Prefix/1
ACACTCTTTCCCTACACGACGCTCTTCCGATCT
>Prefix/2
GTGACTGGAGTTCAGACGTGTGCTCTTCCGATCT
END
` && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR PE \
  -threads 1 \
  -phred33 \
  /project/6007406/efournie/Mnase/raw/HI.4594.004.Index_8.Mnase_WT_YAR845_39C1h_Cl1_R1.fastq.gz \
  /project/6007406/efournie/Mnase/raw/HI.4594.004.Index_8.Mnase_WT_YAR845_39C1h_Cl1_R2.fastq.gz \
  trim/WT_39C_Cl1/HI.4594.004.Index_8.Mnase_WT_YAR845_39C1h_Cl1.trim.pair1.fastq.gz \
  trim/WT_39C_Cl1/HI.4594.004.Index_8.Mnase_WT_YAR845_39C1h_Cl1.trim.single1.fastq.gz \
  trim/WT_39C_Cl1/HI.4594.004.Index_8.Mnase_WT_YAR845_39C1h_Cl1.trim.pair2.fastq.gz \
  trim/WT_39C_Cl1/HI.4594.004.Index_8.Mnase_WT_YAR845_39C1h_Cl1.trim.single2.fastq.gz \
  ILLUMINACLIP:trim/WT_39C_Cl1/HI.4594.004.Index_8.Mnase_WT_YAR845_39C1h_Cl1.trim.adapters.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:50 \
  2> trim/WT_39C_Cl1/HI.4594.004.Index_8.Mnase_WT_YAR845_39C1h_Cl1.trim.log
trimmomatic.HI.4594.004.Index_8.Mnase_WT_YAR845_39C1h_Cl1.0205187a8795c48150bcd76d669d2aa8.mugqic.done
)
trimmomatic_13_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 | grep "[0-9]" | cut -d\  -f4)
echo "$trimmomatic_13_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: trimmomatic_14_JOB_ID: trimmomatic.HI.4594.004.Index_10.Mnase_WT_YAR845_39C1h_Cl3
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.HI.4594.004.Index_10.Mnase_WT_YAR845_39C1h_Cl3
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.HI.4594.004.Index_10.Mnase_WT_YAR845_39C1h_Cl3.4889d84ff33d7b0b8de825e293cef612.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.HI.4594.004.Index_10.Mnase_WT_YAR845_39C1h_Cl3.4889d84ff33d7b0b8de825e293cef612.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/trimmomatic/0.36 && \
mkdir -p trim/WT_39C_Cl3 && \
`cat > trim/WT_39C_Cl3/HI.4594.004.Index_10.Mnase_WT_YAR845_39C1h_Cl3.trim.adapters.fa << END
>Prefix/1
ACACTCTTTCCCTACACGACGCTCTTCCGATCT
>Prefix/2
GTGACTGGAGTTCAGACGTGTGCTCTTCCGATCT
END
` && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR PE \
  -threads 1 \
  -phred33 \
  /project/6007406/efournie/Mnase/raw/HI.4594.004.Index_10.Mnase_WT_YAR845_39C1h_Cl3_R1.fastq.gz \
  /project/6007406/efournie/Mnase/raw/HI.4594.004.Index_10.Mnase_WT_YAR845_39C1h_Cl3_R2.fastq.gz \
  trim/WT_39C_Cl3/HI.4594.004.Index_10.Mnase_WT_YAR845_39C1h_Cl3.trim.pair1.fastq.gz \
  trim/WT_39C_Cl3/HI.4594.004.Index_10.Mnase_WT_YAR845_39C1h_Cl3.trim.single1.fastq.gz \
  trim/WT_39C_Cl3/HI.4594.004.Index_10.Mnase_WT_YAR845_39C1h_Cl3.trim.pair2.fastq.gz \
  trim/WT_39C_Cl3/HI.4594.004.Index_10.Mnase_WT_YAR845_39C1h_Cl3.trim.single2.fastq.gz \
  ILLUMINACLIP:trim/WT_39C_Cl3/HI.4594.004.Index_10.Mnase_WT_YAR845_39C1h_Cl3.trim.adapters.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:50 \
  2> trim/WT_39C_Cl3/HI.4594.004.Index_10.Mnase_WT_YAR845_39C1h_Cl3.trim.log
trimmomatic.HI.4594.004.Index_10.Mnase_WT_YAR845_39C1h_Cl3.4889d84ff33d7b0b8de825e293cef612.mugqic.done
)
trimmomatic_14_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 | grep "[0-9]" | cut -d\  -f4)
echo "$trimmomatic_14_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# STEP: merge_trimmomatic_stats
#-------------------------------------------------------------------------------
STEP=merge_trimmomatic_stats
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: merge_trimmomatic_stats_1_JOB_ID: merge_trimmomatic_stats
#-------------------------------------------------------------------------------
JOB_NAME=merge_trimmomatic_stats
JOB_DEPENDENCIES=$trimmomatic_1_JOB_ID:$trimmomatic_2_JOB_ID:$trimmomatic_3_JOB_ID:$trimmomatic_4_JOB_ID:$trimmomatic_5_JOB_ID:$trimmomatic_6_JOB_ID:$trimmomatic_7_JOB_ID:$trimmomatic_8_JOB_ID:$trimmomatic_9_JOB_ID:$trimmomatic_10_JOB_ID:$trimmomatic_11_JOB_ID:$trimmomatic_12_JOB_ID:$trimmomatic_13_JOB_ID:$trimmomatic_14_JOB_ID
JOB_DONE=job_output/merge_trimmomatic_stats/merge_trimmomatic_stats.1e940e3ee44a21baa4c5ea66dc0f30f1.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'merge_trimmomatic_stats.1e940e3ee44a21baa4c5ea66dc0f30f1.mugqic.done'
module load mugqic/pandoc/1.15.2 && \
mkdir -p metrics && \
echo 'Sample	Readset	Raw Paired Reads #	Surviving Paired Reads #	Surviving Paired Reads %' > metrics/trimReadsetTable.tsv && \
grep ^Input trim/dspt2_30C_Cl2/HI.4594.004.Index_22.Mnase_dspt2_YAR669_Cl2.trim.log | \
perl -pe 's/^Input Read Pairs: (\d+).*Both Surviving: (\d+).*Forward Only Surviving: (\d+).*$/dspt2_30C_Cl2	HI.4594.004.Index_22.Mnase_dspt2_YAR669_Cl2	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/dspt2_30C_Cl3/HI.4594.004.Index_25.Mnase_dspt2_YAR669_Cl3.trim.log | \
perl -pe 's/^Input Read Pairs: (\d+).*Both Surviving: (\d+).*Forward Only Surviving: (\d+).*$/dspt2_30C_Cl3	HI.4594.004.Index_25.Mnase_dspt2_YAR669_Cl3	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/spt2m_30C_Cl2/HI.4594.004.Index_27.Mnase_spt2m_YAR673_Cl2.trim.log | \
perl -pe 's/^Input Read Pairs: (\d+).*Both Surviving: (\d+).*Forward Only Surviving: (\d+).*$/spt2m_30C_Cl2	HI.4594.004.Index_27.Mnase_spt2m_YAR673_Cl2	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/spt2m_30C_Cl3/HI.4594.004.Index_2.Mnase_spt2m_YAR673_Cl3.trim.log | \
perl -pe 's/^Input Read Pairs: (\d+).*Both Surviving: (\d+).*Forward Only Surviving: (\d+).*$/spt2m_30C_Cl3	HI.4594.004.Index_2.Mnase_spt2m_YAR673_Cl3	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/WT_30C_Cl2/HI.4594.004.Index_21.Mnase_WT_YAR670_Cl2.trim.log | \
perl -pe 's/^Input Read Pairs: (\d+).*Both Surviving: (\d+).*Forward Only Surviving: (\d+).*$/WT_30C_Cl2	HI.4594.004.Index_21.Mnase_WT_YAR670_Cl2	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/WT_30C_Cl3/HI.4594.004.Index_23.Mnase_WT_YAR670_Cl3.trim.log | \
perl -pe 's/^Input Read Pairs: (\d+).*Both Surviving: (\d+).*Forward Only Surviving: (\d+).*$/WT_30C_Cl3	HI.4594.004.Index_23.Mnase_WT_YAR670_Cl3	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/CKIIts_37C_Cl1/HI.4594.004.Index_15.Mnase_Exp3_CKIIts_37C2h_Cl1.trim.log | \
perl -pe 's/^Input Read Pairs: (\d+).*Both Surviving: (\d+).*Forward Only Surviving: (\d+).*$/CKIIts_37C_Cl1	HI.4594.004.Index_15.Mnase_Exp3_CKIIts_37C2h_Cl1	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/CKIIts_37C_Cl2/HI.4594.004.Index_7.Mnase_Exp3_CKIIts_37C2h_Cl2.trim.log | \
perl -pe 's/^Input Read Pairs: (\d+).*Both Surviving: (\d+).*Forward Only Surviving: (\d+).*$/CKIIts_37C_Cl2	HI.4594.004.Index_7.Mnase_Exp3_CKIIts_37C2h_Cl2	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/WT_37C_Cl1/HI.4594.004.Index_13.Mnase_Exp3_WT37C2h_Cl1.trim.log | \
perl -pe 's/^Input Read Pairs: (\d+).*Both Surviving: (\d+).*Forward Only Surviving: (\d+).*$/WT_37C_Cl1	HI.4594.004.Index_13.Mnase_Exp3_WT37C2h_Cl1	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/WT_37C_Cl2/HI.4594.004.Index_6.Mnase_Exp3_WT37C2h_Cl2.trim.log | \
perl -pe 's/^Input Read Pairs: (\d+).*Both Surviving: (\d+).*Forward Only Surviving: (\d+).*$/WT_37C_Cl2	HI.4594.004.Index_6.Mnase_Exp3_WT37C2h_Cl2	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/spt6ts_39C_Cl1/HI.4594.004.Index_11.Mnase_spt6ts_YAR844_39C1h_Cl1.trim.log | \
perl -pe 's/^Input Read Pairs: (\d+).*Both Surviving: (\d+).*Forward Only Surviving: (\d+).*$/spt6ts_39C_Cl1	HI.4594.004.Index_11.Mnase_spt6ts_YAR844_39C1h_Cl1	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/spt6ts_39C_Cl2/HI.4594.004.Index_20.Mnase_spt6ts_YAR844_39C1h_Cl2.trim.log | \
perl -pe 's/^Input Read Pairs: (\d+).*Both Surviving: (\d+).*Forward Only Surviving: (\d+).*$/spt6ts_39C_Cl2	HI.4594.004.Index_20.Mnase_spt6ts_YAR844_39C1h_Cl2	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/WT_39C_Cl1/HI.4594.004.Index_8.Mnase_WT_YAR845_39C1h_Cl1.trim.log | \
perl -pe 's/^Input Read Pairs: (\d+).*Both Surviving: (\d+).*Forward Only Surviving: (\d+).*$/WT_39C_Cl1	HI.4594.004.Index_8.Mnase_WT_YAR845_39C1h_Cl1	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/WT_39C_Cl3/HI.4594.004.Index_10.Mnase_WT_YAR845_39C1h_Cl3.trim.log | \
perl -pe 's/^Input Read Pairs: (\d+).*Both Surviving: (\d+).*Forward Only Surviving: (\d+).*$/WT_39C_Cl3	HI.4594.004.Index_10.Mnase_WT_YAR845_39C1h_Cl3	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
cut -f1,3- metrics/trimReadsetTable.tsv | awk -F"	" '{OFS="	"; if (NR==1) {if ($2=="Raw Paired Reads #") {paired=1};print "Sample", "Raw Reads #", "Surviving Reads #", "Surviving %"} else {if (paired) {$2=$2*2; $3=$3*2}; raw[$1]+=$2; surviving[$1]+=$3}}END{for (sample in raw){print sample, raw[sample], surviving[sample], surviving[sample] / raw[sample] * 100}}' \
  > metrics/trimSampleTable.tsv && \
mkdir -p report && \
cp metrics/trimReadsetTable.tsv metrics/trimSampleTable.tsv report/ && \
trim_readset_table_md=`LC_NUMERIC=en_CA awk -F "	" '{OFS="|"; if (NR == 1) {$1 = $1; print $0; print "-----|-----|-----:|-----:|-----:"} else {print $1, $2, sprintf("%\47d", $3), sprintf("%\47d", $4), sprintf("%.1f", $5)}}' metrics/trimReadsetTable.tsv` && \
pandoc \
  /cvmfs/soft.mugqic/CentOS6/software/genpipes/genpipes-3.1.2/bfx/report/Illumina.merge_trimmomatic_stats.md \
  --template /cvmfs/soft.mugqic/CentOS6/software/genpipes/genpipes-3.1.2/bfx/report/Illumina.merge_trimmomatic_stats.md \
  --variable trailing_min_quality=30 \
  --variable min_length=50 \
  --variable read_type=Paired \
  --variable trim_readset_table="$trim_readset_table_md" \
  --to markdown \
  > report/Illumina.merge_trimmomatic_stats.md
merge_trimmomatic_stats.1e940e3ee44a21baa4c5ea66dc0f30f1.mugqic.done
)
merge_trimmomatic_stats_1_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$merge_trimmomatic_stats_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# STEP: bwa_mem_picard_sort_sam
#-------------------------------------------------------------------------------
STEP=bwa_mem_picard_sort_sam
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_1_JOB_ID: bwa_mem_picard_sort_sam.HI.4594.004.Index_22.Mnase_dspt2_YAR669_Cl2
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam.HI.4594.004.Index_22.Mnase_dspt2_YAR669_Cl2
JOB_DEPENDENCIES=$trimmomatic_1_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam.HI.4594.004.Index_22.Mnase_dspt2_YAR669_Cl2.469cc4fd3416d059f9ec1602255d0753.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam.HI.4594.004.Index_22.Mnase_dspt2_YAR669_Cl2.469cc4fd3416d059f9ec1602255d0753.mugqic.done'
module load mugqic/bwa/0.7.12 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/2.0.1 && \
mkdir -p alignment/dspt2_30C_Cl2/HI.4594.004.Index_22.Mnase_dspt2_YAR669_Cl2 && \
bwa mem  \
  -M -t 11 \
  -R '@RG	ID:HI.4594.004.Index_22.Mnase_dspt2_YAR669_Cl2	SM:dspt2_30C_Cl2	LB:dspt2_30C_Cl2	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/bwa_index/Saccharomyces_cerevisiae.R64-1-1.fa \
  trim/dspt2_30C_Cl2/HI.4594.004.Index_22.Mnase_dspt2_YAR669_Cl2.trim.pair1.fastq.gz \
  trim/dspt2_30C_Cl2/HI.4594.004.Index_22.Mnase_dspt2_YAR669_Cl2.trim.pair2.fastq.gz | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx54G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=alignment/dspt2_30C_Cl2/HI.4594.004.Index_22.Mnase_dspt2_YAR669_Cl2/HI.4594.004.Index_22.Mnase_dspt2_YAR669_Cl2.sorted.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=13500000
bwa_mem_picard_sort_sam.HI.4594.004.Index_22.Mnase_dspt2_YAR669_Cl2.469cc4fd3416d059f9ec1602255d0753.mugqic.done
)
bwa_mem_picard_sort_sam_1_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bwa_mem_picard_sort_sam_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_2_JOB_ID: bwa_mem_picard_sort_sam.HI.4594.004.Index_25.Mnase_dspt2_YAR669_Cl3
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam.HI.4594.004.Index_25.Mnase_dspt2_YAR669_Cl3
JOB_DEPENDENCIES=$trimmomatic_2_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam.HI.4594.004.Index_25.Mnase_dspt2_YAR669_Cl3.f230eb4b792271741a50c2cfbe349bd3.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam.HI.4594.004.Index_25.Mnase_dspt2_YAR669_Cl3.f230eb4b792271741a50c2cfbe349bd3.mugqic.done'
module load mugqic/bwa/0.7.12 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/2.0.1 && \
mkdir -p alignment/dspt2_30C_Cl3/HI.4594.004.Index_25.Mnase_dspt2_YAR669_Cl3 && \
bwa mem  \
  -M -t 11 \
  -R '@RG	ID:HI.4594.004.Index_25.Mnase_dspt2_YAR669_Cl3	SM:dspt2_30C_Cl3	LB:dspt2_30C_Cl3	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/bwa_index/Saccharomyces_cerevisiae.R64-1-1.fa \
  trim/dspt2_30C_Cl3/HI.4594.004.Index_25.Mnase_dspt2_YAR669_Cl3.trim.pair1.fastq.gz \
  trim/dspt2_30C_Cl3/HI.4594.004.Index_25.Mnase_dspt2_YAR669_Cl3.trim.pair2.fastq.gz | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx54G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=alignment/dspt2_30C_Cl3/HI.4594.004.Index_25.Mnase_dspt2_YAR669_Cl3/HI.4594.004.Index_25.Mnase_dspt2_YAR669_Cl3.sorted.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=13500000
bwa_mem_picard_sort_sam.HI.4594.004.Index_25.Mnase_dspt2_YAR669_Cl3.f230eb4b792271741a50c2cfbe349bd3.mugqic.done
)
bwa_mem_picard_sort_sam_2_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bwa_mem_picard_sort_sam_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_3_JOB_ID: bwa_mem_picard_sort_sam.HI.4594.004.Index_27.Mnase_spt2m_YAR673_Cl2
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam.HI.4594.004.Index_27.Mnase_spt2m_YAR673_Cl2
JOB_DEPENDENCIES=$trimmomatic_3_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam.HI.4594.004.Index_27.Mnase_spt2m_YAR673_Cl2.9052266716d23df2755378b81e3b6617.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam.HI.4594.004.Index_27.Mnase_spt2m_YAR673_Cl2.9052266716d23df2755378b81e3b6617.mugqic.done'
module load mugqic/bwa/0.7.12 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/2.0.1 && \
mkdir -p alignment/spt2m_30C_Cl2/HI.4594.004.Index_27.Mnase_spt2m_YAR673_Cl2 && \
bwa mem  \
  -M -t 11 \
  -R '@RG	ID:HI.4594.004.Index_27.Mnase_spt2m_YAR673_Cl2	SM:spt2m_30C_Cl2	LB:spt2m_30C_Cl2	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/bwa_index/Saccharomyces_cerevisiae.R64-1-1.fa \
  trim/spt2m_30C_Cl2/HI.4594.004.Index_27.Mnase_spt2m_YAR673_Cl2.trim.pair1.fastq.gz \
  trim/spt2m_30C_Cl2/HI.4594.004.Index_27.Mnase_spt2m_YAR673_Cl2.trim.pair2.fastq.gz | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx54G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=alignment/spt2m_30C_Cl2/HI.4594.004.Index_27.Mnase_spt2m_YAR673_Cl2/HI.4594.004.Index_27.Mnase_spt2m_YAR673_Cl2.sorted.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=13500000
bwa_mem_picard_sort_sam.HI.4594.004.Index_27.Mnase_spt2m_YAR673_Cl2.9052266716d23df2755378b81e3b6617.mugqic.done
)
bwa_mem_picard_sort_sam_3_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bwa_mem_picard_sort_sam_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_4_JOB_ID: bwa_mem_picard_sort_sam.HI.4594.004.Index_2.Mnase_spt2m_YAR673_Cl3
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam.HI.4594.004.Index_2.Mnase_spt2m_YAR673_Cl3
JOB_DEPENDENCIES=$trimmomatic_4_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam.HI.4594.004.Index_2.Mnase_spt2m_YAR673_Cl3.d38eed569b6c88819e4fd4e4d6b43f5a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam.HI.4594.004.Index_2.Mnase_spt2m_YAR673_Cl3.d38eed569b6c88819e4fd4e4d6b43f5a.mugqic.done'
module load mugqic/bwa/0.7.12 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/2.0.1 && \
mkdir -p alignment/spt2m_30C_Cl3/HI.4594.004.Index_2.Mnase_spt2m_YAR673_Cl3 && \
bwa mem  \
  -M -t 11 \
  -R '@RG	ID:HI.4594.004.Index_2.Mnase_spt2m_YAR673_Cl3	SM:spt2m_30C_Cl3	LB:spt2m_30C_Cl3	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/bwa_index/Saccharomyces_cerevisiae.R64-1-1.fa \
  trim/spt2m_30C_Cl3/HI.4594.004.Index_2.Mnase_spt2m_YAR673_Cl3.trim.pair1.fastq.gz \
  trim/spt2m_30C_Cl3/HI.4594.004.Index_2.Mnase_spt2m_YAR673_Cl3.trim.pair2.fastq.gz | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx54G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=alignment/spt2m_30C_Cl3/HI.4594.004.Index_2.Mnase_spt2m_YAR673_Cl3/HI.4594.004.Index_2.Mnase_spt2m_YAR673_Cl3.sorted.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=13500000
bwa_mem_picard_sort_sam.HI.4594.004.Index_2.Mnase_spt2m_YAR673_Cl3.d38eed569b6c88819e4fd4e4d6b43f5a.mugqic.done
)
bwa_mem_picard_sort_sam_4_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bwa_mem_picard_sort_sam_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_5_JOB_ID: bwa_mem_picard_sort_sam.HI.4594.004.Index_21.Mnase_WT_YAR670_Cl2
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam.HI.4594.004.Index_21.Mnase_WT_YAR670_Cl2
JOB_DEPENDENCIES=$trimmomatic_5_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam.HI.4594.004.Index_21.Mnase_WT_YAR670_Cl2.cdd1c15d42a291f332656d74a3b3b7a6.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam.HI.4594.004.Index_21.Mnase_WT_YAR670_Cl2.cdd1c15d42a291f332656d74a3b3b7a6.mugqic.done'
module load mugqic/bwa/0.7.12 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/2.0.1 && \
mkdir -p alignment/WT_30C_Cl2/HI.4594.004.Index_21.Mnase_WT_YAR670_Cl2 && \
bwa mem  \
  -M -t 11 \
  -R '@RG	ID:HI.4594.004.Index_21.Mnase_WT_YAR670_Cl2	SM:WT_30C_Cl2	LB:WT_30C_Cl2	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/bwa_index/Saccharomyces_cerevisiae.R64-1-1.fa \
  trim/WT_30C_Cl2/HI.4594.004.Index_21.Mnase_WT_YAR670_Cl2.trim.pair1.fastq.gz \
  trim/WT_30C_Cl2/HI.4594.004.Index_21.Mnase_WT_YAR670_Cl2.trim.pair2.fastq.gz | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx54G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=alignment/WT_30C_Cl2/HI.4594.004.Index_21.Mnase_WT_YAR670_Cl2/HI.4594.004.Index_21.Mnase_WT_YAR670_Cl2.sorted.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=13500000
bwa_mem_picard_sort_sam.HI.4594.004.Index_21.Mnase_WT_YAR670_Cl2.cdd1c15d42a291f332656d74a3b3b7a6.mugqic.done
)
bwa_mem_picard_sort_sam_5_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bwa_mem_picard_sort_sam_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_6_JOB_ID: bwa_mem_picard_sort_sam.HI.4594.004.Index_23.Mnase_WT_YAR670_Cl3
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam.HI.4594.004.Index_23.Mnase_WT_YAR670_Cl3
JOB_DEPENDENCIES=$trimmomatic_6_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam.HI.4594.004.Index_23.Mnase_WT_YAR670_Cl3.ac78a7ccb437d502130e932282c8e2cc.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam.HI.4594.004.Index_23.Mnase_WT_YAR670_Cl3.ac78a7ccb437d502130e932282c8e2cc.mugqic.done'
module load mugqic/bwa/0.7.12 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/2.0.1 && \
mkdir -p alignment/WT_30C_Cl3/HI.4594.004.Index_23.Mnase_WT_YAR670_Cl3 && \
bwa mem  \
  -M -t 11 \
  -R '@RG	ID:HI.4594.004.Index_23.Mnase_WT_YAR670_Cl3	SM:WT_30C_Cl3	LB:WT_30C_Cl3	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/bwa_index/Saccharomyces_cerevisiae.R64-1-1.fa \
  trim/WT_30C_Cl3/HI.4594.004.Index_23.Mnase_WT_YAR670_Cl3.trim.pair1.fastq.gz \
  trim/WT_30C_Cl3/HI.4594.004.Index_23.Mnase_WT_YAR670_Cl3.trim.pair2.fastq.gz | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx54G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=alignment/WT_30C_Cl3/HI.4594.004.Index_23.Mnase_WT_YAR670_Cl3/HI.4594.004.Index_23.Mnase_WT_YAR670_Cl3.sorted.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=13500000
bwa_mem_picard_sort_sam.HI.4594.004.Index_23.Mnase_WT_YAR670_Cl3.ac78a7ccb437d502130e932282c8e2cc.mugqic.done
)
bwa_mem_picard_sort_sam_6_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bwa_mem_picard_sort_sam_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_7_JOB_ID: bwa_mem_picard_sort_sam.HI.4594.004.Index_15.Mnase_Exp3_CKIIts_37C2h_Cl1
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam.HI.4594.004.Index_15.Mnase_Exp3_CKIIts_37C2h_Cl1
JOB_DEPENDENCIES=$trimmomatic_7_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam.HI.4594.004.Index_15.Mnase_Exp3_CKIIts_37C2h_Cl1.92f2cf71ae6b93f129858bdcc5d438e9.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam.HI.4594.004.Index_15.Mnase_Exp3_CKIIts_37C2h_Cl1.92f2cf71ae6b93f129858bdcc5d438e9.mugqic.done'
module load mugqic/bwa/0.7.12 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/2.0.1 && \
mkdir -p alignment/CKIIts_37C_Cl1/HI.4594.004.Index_15.Mnase_Exp3_CKIIts_37C2h_Cl1 && \
bwa mem  \
  -M -t 11 \
  -R '@RG	ID:HI.4594.004.Index_15.Mnase_Exp3_CKIIts_37C2h_Cl1	SM:CKIIts_37C_Cl1	LB:CKIIts_37C_Cl1	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/bwa_index/Saccharomyces_cerevisiae.R64-1-1.fa \
  trim/CKIIts_37C_Cl1/HI.4594.004.Index_15.Mnase_Exp3_CKIIts_37C2h_Cl1.trim.pair1.fastq.gz \
  trim/CKIIts_37C_Cl1/HI.4594.004.Index_15.Mnase_Exp3_CKIIts_37C2h_Cl1.trim.pair2.fastq.gz | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx54G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=alignment/CKIIts_37C_Cl1/HI.4594.004.Index_15.Mnase_Exp3_CKIIts_37C2h_Cl1/HI.4594.004.Index_15.Mnase_Exp3_CKIIts_37C2h_Cl1.sorted.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=13500000
bwa_mem_picard_sort_sam.HI.4594.004.Index_15.Mnase_Exp3_CKIIts_37C2h_Cl1.92f2cf71ae6b93f129858bdcc5d438e9.mugqic.done
)
bwa_mem_picard_sort_sam_7_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bwa_mem_picard_sort_sam_7_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_8_JOB_ID: bwa_mem_picard_sort_sam.HI.4594.004.Index_7.Mnase_Exp3_CKIIts_37C2h_Cl2
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam.HI.4594.004.Index_7.Mnase_Exp3_CKIIts_37C2h_Cl2
JOB_DEPENDENCIES=$trimmomatic_8_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam.HI.4594.004.Index_7.Mnase_Exp3_CKIIts_37C2h_Cl2.9e92a646695c9395c2c907b87276ad96.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam.HI.4594.004.Index_7.Mnase_Exp3_CKIIts_37C2h_Cl2.9e92a646695c9395c2c907b87276ad96.mugqic.done'
module load mugqic/bwa/0.7.12 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/2.0.1 && \
mkdir -p alignment/CKIIts_37C_Cl2/HI.4594.004.Index_7.Mnase_Exp3_CKIIts_37C2h_Cl2 && \
bwa mem  \
  -M -t 11 \
  -R '@RG	ID:HI.4594.004.Index_7.Mnase_Exp3_CKIIts_37C2h_Cl2	SM:CKIIts_37C_Cl2	LB:CKIIts_37C_Cl2	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/bwa_index/Saccharomyces_cerevisiae.R64-1-1.fa \
  trim/CKIIts_37C_Cl2/HI.4594.004.Index_7.Mnase_Exp3_CKIIts_37C2h_Cl2.trim.pair1.fastq.gz \
  trim/CKIIts_37C_Cl2/HI.4594.004.Index_7.Mnase_Exp3_CKIIts_37C2h_Cl2.trim.pair2.fastq.gz | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx54G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=alignment/CKIIts_37C_Cl2/HI.4594.004.Index_7.Mnase_Exp3_CKIIts_37C2h_Cl2/HI.4594.004.Index_7.Mnase_Exp3_CKIIts_37C2h_Cl2.sorted.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=13500000
bwa_mem_picard_sort_sam.HI.4594.004.Index_7.Mnase_Exp3_CKIIts_37C2h_Cl2.9e92a646695c9395c2c907b87276ad96.mugqic.done
)
bwa_mem_picard_sort_sam_8_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bwa_mem_picard_sort_sam_8_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_9_JOB_ID: bwa_mem_picard_sort_sam.HI.4594.004.Index_13.Mnase_Exp3_WT37C2h_Cl1
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam.HI.4594.004.Index_13.Mnase_Exp3_WT37C2h_Cl1
JOB_DEPENDENCIES=$trimmomatic_9_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam.HI.4594.004.Index_13.Mnase_Exp3_WT37C2h_Cl1.016a6218b04657038076f20ef37fd548.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam.HI.4594.004.Index_13.Mnase_Exp3_WT37C2h_Cl1.016a6218b04657038076f20ef37fd548.mugqic.done'
module load mugqic/bwa/0.7.12 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/2.0.1 && \
mkdir -p alignment/WT_37C_Cl1/HI.4594.004.Index_13.Mnase_Exp3_WT37C2h_Cl1 && \
bwa mem  \
  -M -t 11 \
  -R '@RG	ID:HI.4594.004.Index_13.Mnase_Exp3_WT37C2h_Cl1	SM:WT_37C_Cl1	LB:WT_37C_Cl1	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/bwa_index/Saccharomyces_cerevisiae.R64-1-1.fa \
  trim/WT_37C_Cl1/HI.4594.004.Index_13.Mnase_Exp3_WT37C2h_Cl1.trim.pair1.fastq.gz \
  trim/WT_37C_Cl1/HI.4594.004.Index_13.Mnase_Exp3_WT37C2h_Cl1.trim.pair2.fastq.gz | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx54G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=alignment/WT_37C_Cl1/HI.4594.004.Index_13.Mnase_Exp3_WT37C2h_Cl1/HI.4594.004.Index_13.Mnase_Exp3_WT37C2h_Cl1.sorted.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=13500000
bwa_mem_picard_sort_sam.HI.4594.004.Index_13.Mnase_Exp3_WT37C2h_Cl1.016a6218b04657038076f20ef37fd548.mugqic.done
)
bwa_mem_picard_sort_sam_9_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bwa_mem_picard_sort_sam_9_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_10_JOB_ID: bwa_mem_picard_sort_sam.HI.4594.004.Index_6.Mnase_Exp3_WT37C2h_Cl2
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam.HI.4594.004.Index_6.Mnase_Exp3_WT37C2h_Cl2
JOB_DEPENDENCIES=$trimmomatic_10_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam.HI.4594.004.Index_6.Mnase_Exp3_WT37C2h_Cl2.552fc25fac95d312c1d59d3eafe4fb44.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam.HI.4594.004.Index_6.Mnase_Exp3_WT37C2h_Cl2.552fc25fac95d312c1d59d3eafe4fb44.mugqic.done'
module load mugqic/bwa/0.7.12 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/2.0.1 && \
mkdir -p alignment/WT_37C_Cl2/HI.4594.004.Index_6.Mnase_Exp3_WT37C2h_Cl2 && \
bwa mem  \
  -M -t 11 \
  -R '@RG	ID:HI.4594.004.Index_6.Mnase_Exp3_WT37C2h_Cl2	SM:WT_37C_Cl2	LB:WT_37C_Cl2	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/bwa_index/Saccharomyces_cerevisiae.R64-1-1.fa \
  trim/WT_37C_Cl2/HI.4594.004.Index_6.Mnase_Exp3_WT37C2h_Cl2.trim.pair1.fastq.gz \
  trim/WT_37C_Cl2/HI.4594.004.Index_6.Mnase_Exp3_WT37C2h_Cl2.trim.pair2.fastq.gz | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx54G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=alignment/WT_37C_Cl2/HI.4594.004.Index_6.Mnase_Exp3_WT37C2h_Cl2/HI.4594.004.Index_6.Mnase_Exp3_WT37C2h_Cl2.sorted.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=13500000
bwa_mem_picard_sort_sam.HI.4594.004.Index_6.Mnase_Exp3_WT37C2h_Cl2.552fc25fac95d312c1d59d3eafe4fb44.mugqic.done
)
bwa_mem_picard_sort_sam_10_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bwa_mem_picard_sort_sam_10_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_11_JOB_ID: bwa_mem_picard_sort_sam.HI.4594.004.Index_11.Mnase_spt6ts_YAR844_39C1h_Cl1
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam.HI.4594.004.Index_11.Mnase_spt6ts_YAR844_39C1h_Cl1
JOB_DEPENDENCIES=$trimmomatic_11_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam.HI.4594.004.Index_11.Mnase_spt6ts_YAR844_39C1h_Cl1.d6811cff1422dc33032f41988b1742c8.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam.HI.4594.004.Index_11.Mnase_spt6ts_YAR844_39C1h_Cl1.d6811cff1422dc33032f41988b1742c8.mugqic.done'
module load mugqic/bwa/0.7.12 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/2.0.1 && \
mkdir -p alignment/spt6ts_39C_Cl1/HI.4594.004.Index_11.Mnase_spt6ts_YAR844_39C1h_Cl1 && \
bwa mem  \
  -M -t 11 \
  -R '@RG	ID:HI.4594.004.Index_11.Mnase_spt6ts_YAR844_39C1h_Cl1	SM:spt6ts_39C_Cl1	LB:spt6ts_39C_Cl1	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/bwa_index/Saccharomyces_cerevisiae.R64-1-1.fa \
  trim/spt6ts_39C_Cl1/HI.4594.004.Index_11.Mnase_spt6ts_YAR844_39C1h_Cl1.trim.pair1.fastq.gz \
  trim/spt6ts_39C_Cl1/HI.4594.004.Index_11.Mnase_spt6ts_YAR844_39C1h_Cl1.trim.pair2.fastq.gz | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx54G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=alignment/spt6ts_39C_Cl1/HI.4594.004.Index_11.Mnase_spt6ts_YAR844_39C1h_Cl1/HI.4594.004.Index_11.Mnase_spt6ts_YAR844_39C1h_Cl1.sorted.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=13500000
bwa_mem_picard_sort_sam.HI.4594.004.Index_11.Mnase_spt6ts_YAR844_39C1h_Cl1.d6811cff1422dc33032f41988b1742c8.mugqic.done
)
bwa_mem_picard_sort_sam_11_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bwa_mem_picard_sort_sam_11_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_12_JOB_ID: bwa_mem_picard_sort_sam.HI.4594.004.Index_20.Mnase_spt6ts_YAR844_39C1h_Cl2
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam.HI.4594.004.Index_20.Mnase_spt6ts_YAR844_39C1h_Cl2
JOB_DEPENDENCIES=$trimmomatic_12_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam.HI.4594.004.Index_20.Mnase_spt6ts_YAR844_39C1h_Cl2.bb04ba60669bc59fa6d862fedd13ea66.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam.HI.4594.004.Index_20.Mnase_spt6ts_YAR844_39C1h_Cl2.bb04ba60669bc59fa6d862fedd13ea66.mugqic.done'
module load mugqic/bwa/0.7.12 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/2.0.1 && \
mkdir -p alignment/spt6ts_39C_Cl2/HI.4594.004.Index_20.Mnase_spt6ts_YAR844_39C1h_Cl2 && \
bwa mem  \
  -M -t 11 \
  -R '@RG	ID:HI.4594.004.Index_20.Mnase_spt6ts_YAR844_39C1h_Cl2	SM:spt6ts_39C_Cl2	LB:spt6ts_39C_Cl2	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/bwa_index/Saccharomyces_cerevisiae.R64-1-1.fa \
  trim/spt6ts_39C_Cl2/HI.4594.004.Index_20.Mnase_spt6ts_YAR844_39C1h_Cl2.trim.pair1.fastq.gz \
  trim/spt6ts_39C_Cl2/HI.4594.004.Index_20.Mnase_spt6ts_YAR844_39C1h_Cl2.trim.pair2.fastq.gz | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx54G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=alignment/spt6ts_39C_Cl2/HI.4594.004.Index_20.Mnase_spt6ts_YAR844_39C1h_Cl2/HI.4594.004.Index_20.Mnase_spt6ts_YAR844_39C1h_Cl2.sorted.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=13500000
bwa_mem_picard_sort_sam.HI.4594.004.Index_20.Mnase_spt6ts_YAR844_39C1h_Cl2.bb04ba60669bc59fa6d862fedd13ea66.mugqic.done
)
bwa_mem_picard_sort_sam_12_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bwa_mem_picard_sort_sam_12_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_13_JOB_ID: bwa_mem_picard_sort_sam.HI.4594.004.Index_8.Mnase_WT_YAR845_39C1h_Cl1
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam.HI.4594.004.Index_8.Mnase_WT_YAR845_39C1h_Cl1
JOB_DEPENDENCIES=$trimmomatic_13_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam.HI.4594.004.Index_8.Mnase_WT_YAR845_39C1h_Cl1.f9a22eafec67357a44e0c486f28134e1.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam.HI.4594.004.Index_8.Mnase_WT_YAR845_39C1h_Cl1.f9a22eafec67357a44e0c486f28134e1.mugqic.done'
module load mugqic/bwa/0.7.12 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/2.0.1 && \
mkdir -p alignment/WT_39C_Cl1/HI.4594.004.Index_8.Mnase_WT_YAR845_39C1h_Cl1 && \
bwa mem  \
  -M -t 11 \
  -R '@RG	ID:HI.4594.004.Index_8.Mnase_WT_YAR845_39C1h_Cl1	SM:WT_39C_Cl1	LB:WT_39C_Cl1	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/bwa_index/Saccharomyces_cerevisiae.R64-1-1.fa \
  trim/WT_39C_Cl1/HI.4594.004.Index_8.Mnase_WT_YAR845_39C1h_Cl1.trim.pair1.fastq.gz \
  trim/WT_39C_Cl1/HI.4594.004.Index_8.Mnase_WT_YAR845_39C1h_Cl1.trim.pair2.fastq.gz | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx54G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=alignment/WT_39C_Cl1/HI.4594.004.Index_8.Mnase_WT_YAR845_39C1h_Cl1/HI.4594.004.Index_8.Mnase_WT_YAR845_39C1h_Cl1.sorted.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=13500000
bwa_mem_picard_sort_sam.HI.4594.004.Index_8.Mnase_WT_YAR845_39C1h_Cl1.f9a22eafec67357a44e0c486f28134e1.mugqic.done
)
bwa_mem_picard_sort_sam_13_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bwa_mem_picard_sort_sam_13_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_14_JOB_ID: bwa_mem_picard_sort_sam.HI.4594.004.Index_10.Mnase_WT_YAR845_39C1h_Cl3
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam.HI.4594.004.Index_10.Mnase_WT_YAR845_39C1h_Cl3
JOB_DEPENDENCIES=$trimmomatic_14_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam.HI.4594.004.Index_10.Mnase_WT_YAR845_39C1h_Cl3.2f6c8b6c680256d80c69e2fa3a2e3247.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam.HI.4594.004.Index_10.Mnase_WT_YAR845_39C1h_Cl3.2f6c8b6c680256d80c69e2fa3a2e3247.mugqic.done'
module load mugqic/bwa/0.7.12 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/2.0.1 && \
mkdir -p alignment/WT_39C_Cl3/HI.4594.004.Index_10.Mnase_WT_YAR845_39C1h_Cl3 && \
bwa mem  \
  -M -t 11 \
  -R '@RG	ID:HI.4594.004.Index_10.Mnase_WT_YAR845_39C1h_Cl3	SM:WT_39C_Cl3	LB:WT_39C_Cl3	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Saccharomyces_cerevisiae.R64-1-1/genome/bwa_index/Saccharomyces_cerevisiae.R64-1-1.fa \
  trim/WT_39C_Cl3/HI.4594.004.Index_10.Mnase_WT_YAR845_39C1h_Cl3.trim.pair1.fastq.gz \
  trim/WT_39C_Cl3/HI.4594.004.Index_10.Mnase_WT_YAR845_39C1h_Cl3.trim.pair2.fastq.gz | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx54G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=alignment/WT_39C_Cl3/HI.4594.004.Index_10.Mnase_WT_YAR845_39C1h_Cl3/HI.4594.004.Index_10.Mnase_WT_YAR845_39C1h_Cl3.sorted.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=13500000
bwa_mem_picard_sort_sam.HI.4594.004.Index_10.Mnase_WT_YAR845_39C1h_Cl3.2f6c8b6c680256d80c69e2fa3a2e3247.mugqic.done
)
bwa_mem_picard_sort_sam_14_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bwa_mem_picard_sort_sam_14_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_15_JOB_ID: bwa_mem_picard_sort_sam_report
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam_report
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_1_JOB_ID:$bwa_mem_picard_sort_sam_2_JOB_ID:$bwa_mem_picard_sort_sam_3_JOB_ID:$bwa_mem_picard_sort_sam_4_JOB_ID:$bwa_mem_picard_sort_sam_5_JOB_ID:$bwa_mem_picard_sort_sam_6_JOB_ID:$bwa_mem_picard_sort_sam_7_JOB_ID:$bwa_mem_picard_sort_sam_8_JOB_ID:$bwa_mem_picard_sort_sam_9_JOB_ID:$bwa_mem_picard_sort_sam_10_JOB_ID:$bwa_mem_picard_sort_sam_11_JOB_ID:$bwa_mem_picard_sort_sam_12_JOB_ID:$bwa_mem_picard_sort_sam_13_JOB_ID:$bwa_mem_picard_sort_sam_14_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam_report.c90996efde9d2a30c7362cb9c68f0c37.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam_report.c90996efde9d2a30c7362cb9c68f0c37.mugqic.done'
module load mugqic/pandoc/1.15.2 && \
mkdir -p report && \
pandoc --to=markdown \
  --template /cvmfs/soft.mugqic/CentOS6/software/genpipes/genpipes-3.1.2/bfx/report/DnaSeq.bwa_mem_picard_sort_sam.md \
  --variable scientific_name="Saccharomyces_cerevisiae" \
  --variable assembly="R64-1-1" \
  /cvmfs/soft.mugqic/CentOS6/software/genpipes/genpipes-3.1.2/bfx/report/DnaSeq.bwa_mem_picard_sort_sam.md \
  > report/DnaSeq.bwa_mem_picard_sort_sam.md
bwa_mem_picard_sort_sam_report.c90996efde9d2a30c7362cb9c68f0c37.mugqic.done
)
bwa_mem_picard_sort_sam_15_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bwa_mem_picard_sort_sam_15_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# STEP: samtools_view_filter
#-------------------------------------------------------------------------------
STEP=samtools_view_filter
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_1_JOB_ID: samtools_view_filter.HI.4594.004.Index_22.Mnase_dspt2_YAR669_Cl2
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.HI.4594.004.Index_22.Mnase_dspt2_YAR669_Cl2
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_1_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.HI.4594.004.Index_22.Mnase_dspt2_YAR669_Cl2.fa275a0f3752bccedd5657b242e866bf.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.HI.4594.004.Index_22.Mnase_dspt2_YAR669_Cl2.fa275a0f3752bccedd5657b242e866bf.mugqic.done'
module load mugqic/samtools/1.3.1 && \
samtools view -b -F4 -q 20 \
  alignment/dspt2_30C_Cl2/HI.4594.004.Index_22.Mnase_dspt2_YAR669_Cl2/HI.4594.004.Index_22.Mnase_dspt2_YAR669_Cl2.sorted.bam \
  > alignment/dspt2_30C_Cl2/HI.4594.004.Index_22.Mnase_dspt2_YAR669_Cl2/HI.4594.004.Index_22.Mnase_dspt2_YAR669_Cl2.sorted.filtered.bam
samtools_view_filter.HI.4594.004.Index_22.Mnase_dspt2_YAR669_Cl2.fa275a0f3752bccedd5657b242e866bf.mugqic.done
)
samtools_view_filter_1_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$samtools_view_filter_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_2_JOB_ID: samtools_view_filter.HI.4594.004.Index_25.Mnase_dspt2_YAR669_Cl3
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.HI.4594.004.Index_25.Mnase_dspt2_YAR669_Cl3
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_2_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.HI.4594.004.Index_25.Mnase_dspt2_YAR669_Cl3.b45b2948188adb18d294f2e1da87d020.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.HI.4594.004.Index_25.Mnase_dspt2_YAR669_Cl3.b45b2948188adb18d294f2e1da87d020.mugqic.done'
module load mugqic/samtools/1.3.1 && \
samtools view -b -F4 -q 20 \
  alignment/dspt2_30C_Cl3/HI.4594.004.Index_25.Mnase_dspt2_YAR669_Cl3/HI.4594.004.Index_25.Mnase_dspt2_YAR669_Cl3.sorted.bam \
  > alignment/dspt2_30C_Cl3/HI.4594.004.Index_25.Mnase_dspt2_YAR669_Cl3/HI.4594.004.Index_25.Mnase_dspt2_YAR669_Cl3.sorted.filtered.bam
samtools_view_filter.HI.4594.004.Index_25.Mnase_dspt2_YAR669_Cl3.b45b2948188adb18d294f2e1da87d020.mugqic.done
)
samtools_view_filter_2_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$samtools_view_filter_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_3_JOB_ID: samtools_view_filter.HI.4594.004.Index_27.Mnase_spt2m_YAR673_Cl2
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.HI.4594.004.Index_27.Mnase_spt2m_YAR673_Cl2
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_3_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.HI.4594.004.Index_27.Mnase_spt2m_YAR673_Cl2.125b1d391c7b7df19eb1fec2cacb9c18.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.HI.4594.004.Index_27.Mnase_spt2m_YAR673_Cl2.125b1d391c7b7df19eb1fec2cacb9c18.mugqic.done'
module load mugqic/samtools/1.3.1 && \
samtools view -b -F4 -q 20 \
  alignment/spt2m_30C_Cl2/HI.4594.004.Index_27.Mnase_spt2m_YAR673_Cl2/HI.4594.004.Index_27.Mnase_spt2m_YAR673_Cl2.sorted.bam \
  > alignment/spt2m_30C_Cl2/HI.4594.004.Index_27.Mnase_spt2m_YAR673_Cl2/HI.4594.004.Index_27.Mnase_spt2m_YAR673_Cl2.sorted.filtered.bam
samtools_view_filter.HI.4594.004.Index_27.Mnase_spt2m_YAR673_Cl2.125b1d391c7b7df19eb1fec2cacb9c18.mugqic.done
)
samtools_view_filter_3_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$samtools_view_filter_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_4_JOB_ID: samtools_view_filter.HI.4594.004.Index_2.Mnase_spt2m_YAR673_Cl3
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.HI.4594.004.Index_2.Mnase_spt2m_YAR673_Cl3
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_4_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.HI.4594.004.Index_2.Mnase_spt2m_YAR673_Cl3.3ecc343585d7ec3744e1e981fc7f53da.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.HI.4594.004.Index_2.Mnase_spt2m_YAR673_Cl3.3ecc343585d7ec3744e1e981fc7f53da.mugqic.done'
module load mugqic/samtools/1.3.1 && \
samtools view -b -F4 -q 20 \
  alignment/spt2m_30C_Cl3/HI.4594.004.Index_2.Mnase_spt2m_YAR673_Cl3/HI.4594.004.Index_2.Mnase_spt2m_YAR673_Cl3.sorted.bam \
  > alignment/spt2m_30C_Cl3/HI.4594.004.Index_2.Mnase_spt2m_YAR673_Cl3/HI.4594.004.Index_2.Mnase_spt2m_YAR673_Cl3.sorted.filtered.bam
samtools_view_filter.HI.4594.004.Index_2.Mnase_spt2m_YAR673_Cl3.3ecc343585d7ec3744e1e981fc7f53da.mugqic.done
)
samtools_view_filter_4_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$samtools_view_filter_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_5_JOB_ID: samtools_view_filter.HI.4594.004.Index_21.Mnase_WT_YAR670_Cl2
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.HI.4594.004.Index_21.Mnase_WT_YAR670_Cl2
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_5_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.HI.4594.004.Index_21.Mnase_WT_YAR670_Cl2.ffbdb8e21bed11407d7fb59346dc02d2.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.HI.4594.004.Index_21.Mnase_WT_YAR670_Cl2.ffbdb8e21bed11407d7fb59346dc02d2.mugqic.done'
module load mugqic/samtools/1.3.1 && \
samtools view -b -F4 -q 20 \
  alignment/WT_30C_Cl2/HI.4594.004.Index_21.Mnase_WT_YAR670_Cl2/HI.4594.004.Index_21.Mnase_WT_YAR670_Cl2.sorted.bam \
  > alignment/WT_30C_Cl2/HI.4594.004.Index_21.Mnase_WT_YAR670_Cl2/HI.4594.004.Index_21.Mnase_WT_YAR670_Cl2.sorted.filtered.bam
samtools_view_filter.HI.4594.004.Index_21.Mnase_WT_YAR670_Cl2.ffbdb8e21bed11407d7fb59346dc02d2.mugqic.done
)
samtools_view_filter_5_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$samtools_view_filter_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_6_JOB_ID: samtools_view_filter.HI.4594.004.Index_23.Mnase_WT_YAR670_Cl3
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.HI.4594.004.Index_23.Mnase_WT_YAR670_Cl3
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_6_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.HI.4594.004.Index_23.Mnase_WT_YAR670_Cl3.7d6e9ccba9cfe717f8e8709449521f5a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.HI.4594.004.Index_23.Mnase_WT_YAR670_Cl3.7d6e9ccba9cfe717f8e8709449521f5a.mugqic.done'
module load mugqic/samtools/1.3.1 && \
samtools view -b -F4 -q 20 \
  alignment/WT_30C_Cl3/HI.4594.004.Index_23.Mnase_WT_YAR670_Cl3/HI.4594.004.Index_23.Mnase_WT_YAR670_Cl3.sorted.bam \
  > alignment/WT_30C_Cl3/HI.4594.004.Index_23.Mnase_WT_YAR670_Cl3/HI.4594.004.Index_23.Mnase_WT_YAR670_Cl3.sorted.filtered.bam
samtools_view_filter.HI.4594.004.Index_23.Mnase_WT_YAR670_Cl3.7d6e9ccba9cfe717f8e8709449521f5a.mugqic.done
)
samtools_view_filter_6_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$samtools_view_filter_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_7_JOB_ID: samtools_view_filter.HI.4594.004.Index_15.Mnase_Exp3_CKIIts_37C2h_Cl1
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.HI.4594.004.Index_15.Mnase_Exp3_CKIIts_37C2h_Cl1
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_7_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.HI.4594.004.Index_15.Mnase_Exp3_CKIIts_37C2h_Cl1.3666a0c115ba3295a55fb183f4c16b23.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.HI.4594.004.Index_15.Mnase_Exp3_CKIIts_37C2h_Cl1.3666a0c115ba3295a55fb183f4c16b23.mugqic.done'
module load mugqic/samtools/1.3.1 && \
samtools view -b -F4 -q 20 \
  alignment/CKIIts_37C_Cl1/HI.4594.004.Index_15.Mnase_Exp3_CKIIts_37C2h_Cl1/HI.4594.004.Index_15.Mnase_Exp3_CKIIts_37C2h_Cl1.sorted.bam \
  > alignment/CKIIts_37C_Cl1/HI.4594.004.Index_15.Mnase_Exp3_CKIIts_37C2h_Cl1/HI.4594.004.Index_15.Mnase_Exp3_CKIIts_37C2h_Cl1.sorted.filtered.bam
samtools_view_filter.HI.4594.004.Index_15.Mnase_Exp3_CKIIts_37C2h_Cl1.3666a0c115ba3295a55fb183f4c16b23.mugqic.done
)
samtools_view_filter_7_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$samtools_view_filter_7_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_8_JOB_ID: samtools_view_filter.HI.4594.004.Index_7.Mnase_Exp3_CKIIts_37C2h_Cl2
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.HI.4594.004.Index_7.Mnase_Exp3_CKIIts_37C2h_Cl2
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_8_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.HI.4594.004.Index_7.Mnase_Exp3_CKIIts_37C2h_Cl2.9f03e1dc56df69a17020960195f90c7e.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.HI.4594.004.Index_7.Mnase_Exp3_CKIIts_37C2h_Cl2.9f03e1dc56df69a17020960195f90c7e.mugqic.done'
module load mugqic/samtools/1.3.1 && \
samtools view -b -F4 -q 20 \
  alignment/CKIIts_37C_Cl2/HI.4594.004.Index_7.Mnase_Exp3_CKIIts_37C2h_Cl2/HI.4594.004.Index_7.Mnase_Exp3_CKIIts_37C2h_Cl2.sorted.bam \
  > alignment/CKIIts_37C_Cl2/HI.4594.004.Index_7.Mnase_Exp3_CKIIts_37C2h_Cl2/HI.4594.004.Index_7.Mnase_Exp3_CKIIts_37C2h_Cl2.sorted.filtered.bam
samtools_view_filter.HI.4594.004.Index_7.Mnase_Exp3_CKIIts_37C2h_Cl2.9f03e1dc56df69a17020960195f90c7e.mugqic.done
)
samtools_view_filter_8_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$samtools_view_filter_8_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_9_JOB_ID: samtools_view_filter.HI.4594.004.Index_13.Mnase_Exp3_WT37C2h_Cl1
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.HI.4594.004.Index_13.Mnase_Exp3_WT37C2h_Cl1
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_9_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.HI.4594.004.Index_13.Mnase_Exp3_WT37C2h_Cl1.232969575e8345b8b4cd8e0b4389f70f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.HI.4594.004.Index_13.Mnase_Exp3_WT37C2h_Cl1.232969575e8345b8b4cd8e0b4389f70f.mugqic.done'
module load mugqic/samtools/1.3.1 && \
samtools view -b -F4 -q 20 \
  alignment/WT_37C_Cl1/HI.4594.004.Index_13.Mnase_Exp3_WT37C2h_Cl1/HI.4594.004.Index_13.Mnase_Exp3_WT37C2h_Cl1.sorted.bam \
  > alignment/WT_37C_Cl1/HI.4594.004.Index_13.Mnase_Exp3_WT37C2h_Cl1/HI.4594.004.Index_13.Mnase_Exp3_WT37C2h_Cl1.sorted.filtered.bam
samtools_view_filter.HI.4594.004.Index_13.Mnase_Exp3_WT37C2h_Cl1.232969575e8345b8b4cd8e0b4389f70f.mugqic.done
)
samtools_view_filter_9_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$samtools_view_filter_9_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_10_JOB_ID: samtools_view_filter.HI.4594.004.Index_6.Mnase_Exp3_WT37C2h_Cl2
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.HI.4594.004.Index_6.Mnase_Exp3_WT37C2h_Cl2
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_10_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.HI.4594.004.Index_6.Mnase_Exp3_WT37C2h_Cl2.7a5cb4810f2a6a72307f0cf7ecf947fe.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.HI.4594.004.Index_6.Mnase_Exp3_WT37C2h_Cl2.7a5cb4810f2a6a72307f0cf7ecf947fe.mugqic.done'
module load mugqic/samtools/1.3.1 && \
samtools view -b -F4 -q 20 \
  alignment/WT_37C_Cl2/HI.4594.004.Index_6.Mnase_Exp3_WT37C2h_Cl2/HI.4594.004.Index_6.Mnase_Exp3_WT37C2h_Cl2.sorted.bam \
  > alignment/WT_37C_Cl2/HI.4594.004.Index_6.Mnase_Exp3_WT37C2h_Cl2/HI.4594.004.Index_6.Mnase_Exp3_WT37C2h_Cl2.sorted.filtered.bam
samtools_view_filter.HI.4594.004.Index_6.Mnase_Exp3_WT37C2h_Cl2.7a5cb4810f2a6a72307f0cf7ecf947fe.mugqic.done
)
samtools_view_filter_10_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$samtools_view_filter_10_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_11_JOB_ID: samtools_view_filter.HI.4594.004.Index_11.Mnase_spt6ts_YAR844_39C1h_Cl1
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.HI.4594.004.Index_11.Mnase_spt6ts_YAR844_39C1h_Cl1
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_11_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.HI.4594.004.Index_11.Mnase_spt6ts_YAR844_39C1h_Cl1.0fc7dee259b2136673990969592c6f22.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.HI.4594.004.Index_11.Mnase_spt6ts_YAR844_39C1h_Cl1.0fc7dee259b2136673990969592c6f22.mugqic.done'
module load mugqic/samtools/1.3.1 && \
samtools view -b -F4 -q 20 \
  alignment/spt6ts_39C_Cl1/HI.4594.004.Index_11.Mnase_spt6ts_YAR844_39C1h_Cl1/HI.4594.004.Index_11.Mnase_spt6ts_YAR844_39C1h_Cl1.sorted.bam \
  > alignment/spt6ts_39C_Cl1/HI.4594.004.Index_11.Mnase_spt6ts_YAR844_39C1h_Cl1/HI.4594.004.Index_11.Mnase_spt6ts_YAR844_39C1h_Cl1.sorted.filtered.bam
samtools_view_filter.HI.4594.004.Index_11.Mnase_spt6ts_YAR844_39C1h_Cl1.0fc7dee259b2136673990969592c6f22.mugqic.done
)
samtools_view_filter_11_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$samtools_view_filter_11_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_12_JOB_ID: samtools_view_filter.HI.4594.004.Index_20.Mnase_spt6ts_YAR844_39C1h_Cl2
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.HI.4594.004.Index_20.Mnase_spt6ts_YAR844_39C1h_Cl2
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_12_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.HI.4594.004.Index_20.Mnase_spt6ts_YAR844_39C1h_Cl2.c9c5081cc301ca0b485c46333c216856.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.HI.4594.004.Index_20.Mnase_spt6ts_YAR844_39C1h_Cl2.c9c5081cc301ca0b485c46333c216856.mugqic.done'
module load mugqic/samtools/1.3.1 && \
samtools view -b -F4 -q 20 \
  alignment/spt6ts_39C_Cl2/HI.4594.004.Index_20.Mnase_spt6ts_YAR844_39C1h_Cl2/HI.4594.004.Index_20.Mnase_spt6ts_YAR844_39C1h_Cl2.sorted.bam \
  > alignment/spt6ts_39C_Cl2/HI.4594.004.Index_20.Mnase_spt6ts_YAR844_39C1h_Cl2/HI.4594.004.Index_20.Mnase_spt6ts_YAR844_39C1h_Cl2.sorted.filtered.bam
samtools_view_filter.HI.4594.004.Index_20.Mnase_spt6ts_YAR844_39C1h_Cl2.c9c5081cc301ca0b485c46333c216856.mugqic.done
)
samtools_view_filter_12_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$samtools_view_filter_12_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_13_JOB_ID: samtools_view_filter.HI.4594.004.Index_8.Mnase_WT_YAR845_39C1h_Cl1
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.HI.4594.004.Index_8.Mnase_WT_YAR845_39C1h_Cl1
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_13_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.HI.4594.004.Index_8.Mnase_WT_YAR845_39C1h_Cl1.31ae811d8587421df88d598ee3c6ff79.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.HI.4594.004.Index_8.Mnase_WT_YAR845_39C1h_Cl1.31ae811d8587421df88d598ee3c6ff79.mugqic.done'
module load mugqic/samtools/1.3.1 && \
samtools view -b -F4 -q 20 \
  alignment/WT_39C_Cl1/HI.4594.004.Index_8.Mnase_WT_YAR845_39C1h_Cl1/HI.4594.004.Index_8.Mnase_WT_YAR845_39C1h_Cl1.sorted.bam \
  > alignment/WT_39C_Cl1/HI.4594.004.Index_8.Mnase_WT_YAR845_39C1h_Cl1/HI.4594.004.Index_8.Mnase_WT_YAR845_39C1h_Cl1.sorted.filtered.bam
samtools_view_filter.HI.4594.004.Index_8.Mnase_WT_YAR845_39C1h_Cl1.31ae811d8587421df88d598ee3c6ff79.mugqic.done
)
samtools_view_filter_13_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$samtools_view_filter_13_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_14_JOB_ID: samtools_view_filter.HI.4594.004.Index_10.Mnase_WT_YAR845_39C1h_Cl3
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.HI.4594.004.Index_10.Mnase_WT_YAR845_39C1h_Cl3
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_14_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.HI.4594.004.Index_10.Mnase_WT_YAR845_39C1h_Cl3.21091d6b14ce2c687a4352c031a75372.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.HI.4594.004.Index_10.Mnase_WT_YAR845_39C1h_Cl3.21091d6b14ce2c687a4352c031a75372.mugqic.done'
module load mugqic/samtools/1.3.1 && \
samtools view -b -F4 -q 20 \
  alignment/WT_39C_Cl3/HI.4594.004.Index_10.Mnase_WT_YAR845_39C1h_Cl3/HI.4594.004.Index_10.Mnase_WT_YAR845_39C1h_Cl3.sorted.bam \
  > alignment/WT_39C_Cl3/HI.4594.004.Index_10.Mnase_WT_YAR845_39C1h_Cl3/HI.4594.004.Index_10.Mnase_WT_YAR845_39C1h_Cl3.sorted.filtered.bam
samtools_view_filter.HI.4594.004.Index_10.Mnase_WT_YAR845_39C1h_Cl3.21091d6b14ce2c687a4352c031a75372.mugqic.done
)
samtools_view_filter_14_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$samtools_view_filter_14_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_15_JOB_ID: samtools_view_filter_report
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter_report
JOB_DEPENDENCIES=$samtools_view_filter_1_JOB_ID:$samtools_view_filter_2_JOB_ID:$samtools_view_filter_3_JOB_ID:$samtools_view_filter_4_JOB_ID:$samtools_view_filter_5_JOB_ID:$samtools_view_filter_6_JOB_ID:$samtools_view_filter_7_JOB_ID:$samtools_view_filter_8_JOB_ID:$samtools_view_filter_9_JOB_ID:$samtools_view_filter_10_JOB_ID:$samtools_view_filter_11_JOB_ID:$samtools_view_filter_12_JOB_ID:$samtools_view_filter_13_JOB_ID:$samtools_view_filter_14_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter_report.8d06991c14c95c9c183f78f67effc8fb.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter_report.8d06991c14c95c9c183f78f67effc8fb.mugqic.done'
module load mugqic/pandoc/1.15.2 && \
mkdir -p report && \
pandoc --to=markdown \
  --template /cvmfs/soft.mugqic/CentOS6/software/genpipes/genpipes-3.1.2/bfx/report/ChipSeq.samtools_view_filter.md \
  --variable min_mapq="20" \
  /cvmfs/soft.mugqic/CentOS6/software/genpipes/genpipes-3.1.2/bfx/report/ChipSeq.samtools_view_filter.md \
  > report/ChipSeq.samtools_view_filter.md
samtools_view_filter_report.8d06991c14c95c9c183f78f67effc8fb.mugqic.done
)
samtools_view_filter_15_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$samtools_view_filter_15_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# STEP: picard_merge_sam_files
#-------------------------------------------------------------------------------
STEP=picard_merge_sam_files
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_1_JOB_ID: symlink_readset_sample_bam.dspt2_30C_Cl2
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.dspt2_30C_Cl2
JOB_DEPENDENCIES=$samtools_view_filter_1_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.dspt2_30C_Cl2.8b62d30db37a537229127a47296d5e32.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.dspt2_30C_Cl2.8b62d30db37a537229127a47296d5e32.mugqic.done'
mkdir -p alignment/dspt2_30C_Cl2 && \
ln -s -f HI.4594.004.Index_22.Mnase_dspt2_YAR669_Cl2/HI.4594.004.Index_22.Mnase_dspt2_YAR669_Cl2.sorted.filtered.bam alignment/dspt2_30C_Cl2/dspt2_30C_Cl2.merged.bam
symlink_readset_sample_bam.dspt2_30C_Cl2.8b62d30db37a537229127a47296d5e32.mugqic.done
)
picard_merge_sam_files_1_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_merge_sam_files_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_2_JOB_ID: symlink_readset_sample_bam.dspt2_30C_Cl3
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.dspt2_30C_Cl3
JOB_DEPENDENCIES=$samtools_view_filter_2_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.dspt2_30C_Cl3.5e3eac1f9e252590a9aa9dd7663b926c.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.dspt2_30C_Cl3.5e3eac1f9e252590a9aa9dd7663b926c.mugqic.done'
mkdir -p alignment/dspt2_30C_Cl3 && \
ln -s -f HI.4594.004.Index_25.Mnase_dspt2_YAR669_Cl3/HI.4594.004.Index_25.Mnase_dspt2_YAR669_Cl3.sorted.filtered.bam alignment/dspt2_30C_Cl3/dspt2_30C_Cl3.merged.bam
symlink_readset_sample_bam.dspt2_30C_Cl3.5e3eac1f9e252590a9aa9dd7663b926c.mugqic.done
)
picard_merge_sam_files_2_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_merge_sam_files_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_3_JOB_ID: symlink_readset_sample_bam.spt2m_30C_Cl2
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.spt2m_30C_Cl2
JOB_DEPENDENCIES=$samtools_view_filter_3_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.spt2m_30C_Cl2.0d05e029ac04f9af31f9de5473448aa3.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.spt2m_30C_Cl2.0d05e029ac04f9af31f9de5473448aa3.mugqic.done'
mkdir -p alignment/spt2m_30C_Cl2 && \
ln -s -f HI.4594.004.Index_27.Mnase_spt2m_YAR673_Cl2/HI.4594.004.Index_27.Mnase_spt2m_YAR673_Cl2.sorted.filtered.bam alignment/spt2m_30C_Cl2/spt2m_30C_Cl2.merged.bam
symlink_readset_sample_bam.spt2m_30C_Cl2.0d05e029ac04f9af31f9de5473448aa3.mugqic.done
)
picard_merge_sam_files_3_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_merge_sam_files_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_4_JOB_ID: symlink_readset_sample_bam.spt2m_30C_Cl3
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.spt2m_30C_Cl3
JOB_DEPENDENCIES=$samtools_view_filter_4_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.spt2m_30C_Cl3.5ddda9deb87225cacdbc6f97660b0729.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.spt2m_30C_Cl3.5ddda9deb87225cacdbc6f97660b0729.mugqic.done'
mkdir -p alignment/spt2m_30C_Cl3 && \
ln -s -f HI.4594.004.Index_2.Mnase_spt2m_YAR673_Cl3/HI.4594.004.Index_2.Mnase_spt2m_YAR673_Cl3.sorted.filtered.bam alignment/spt2m_30C_Cl3/spt2m_30C_Cl3.merged.bam
symlink_readset_sample_bam.spt2m_30C_Cl3.5ddda9deb87225cacdbc6f97660b0729.mugqic.done
)
picard_merge_sam_files_4_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_merge_sam_files_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_5_JOB_ID: symlink_readset_sample_bam.WT_30C_Cl2
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.WT_30C_Cl2
JOB_DEPENDENCIES=$samtools_view_filter_5_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.WT_30C_Cl2.e52d7da118968e887d7adba77b0e3683.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.WT_30C_Cl2.e52d7da118968e887d7adba77b0e3683.mugqic.done'
mkdir -p alignment/WT_30C_Cl2 && \
ln -s -f HI.4594.004.Index_21.Mnase_WT_YAR670_Cl2/HI.4594.004.Index_21.Mnase_WT_YAR670_Cl2.sorted.filtered.bam alignment/WT_30C_Cl2/WT_30C_Cl2.merged.bam
symlink_readset_sample_bam.WT_30C_Cl2.e52d7da118968e887d7adba77b0e3683.mugqic.done
)
picard_merge_sam_files_5_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_merge_sam_files_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_6_JOB_ID: symlink_readset_sample_bam.WT_30C_Cl3
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.WT_30C_Cl3
JOB_DEPENDENCIES=$samtools_view_filter_6_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.WT_30C_Cl3.b71a37eb9bcebdf94d8c08d3f48ff598.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.WT_30C_Cl3.b71a37eb9bcebdf94d8c08d3f48ff598.mugqic.done'
mkdir -p alignment/WT_30C_Cl3 && \
ln -s -f HI.4594.004.Index_23.Mnase_WT_YAR670_Cl3/HI.4594.004.Index_23.Mnase_WT_YAR670_Cl3.sorted.filtered.bam alignment/WT_30C_Cl3/WT_30C_Cl3.merged.bam
symlink_readset_sample_bam.WT_30C_Cl3.b71a37eb9bcebdf94d8c08d3f48ff598.mugqic.done
)
picard_merge_sam_files_6_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_merge_sam_files_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_7_JOB_ID: symlink_readset_sample_bam.CKIIts_37C_Cl1
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.CKIIts_37C_Cl1
JOB_DEPENDENCIES=$samtools_view_filter_7_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.CKIIts_37C_Cl1.2b5c631c2ec1191b89b894555bb232ac.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.CKIIts_37C_Cl1.2b5c631c2ec1191b89b894555bb232ac.mugqic.done'
mkdir -p alignment/CKIIts_37C_Cl1 && \
ln -s -f HI.4594.004.Index_15.Mnase_Exp3_CKIIts_37C2h_Cl1/HI.4594.004.Index_15.Mnase_Exp3_CKIIts_37C2h_Cl1.sorted.filtered.bam alignment/CKIIts_37C_Cl1/CKIIts_37C_Cl1.merged.bam
symlink_readset_sample_bam.CKIIts_37C_Cl1.2b5c631c2ec1191b89b894555bb232ac.mugqic.done
)
picard_merge_sam_files_7_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_merge_sam_files_7_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_8_JOB_ID: symlink_readset_sample_bam.CKIIts_37C_Cl2
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.CKIIts_37C_Cl2
JOB_DEPENDENCIES=$samtools_view_filter_8_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.CKIIts_37C_Cl2.7893590e47fd87ad9e01ec7afa45898a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.CKIIts_37C_Cl2.7893590e47fd87ad9e01ec7afa45898a.mugqic.done'
mkdir -p alignment/CKIIts_37C_Cl2 && \
ln -s -f HI.4594.004.Index_7.Mnase_Exp3_CKIIts_37C2h_Cl2/HI.4594.004.Index_7.Mnase_Exp3_CKIIts_37C2h_Cl2.sorted.filtered.bam alignment/CKIIts_37C_Cl2/CKIIts_37C_Cl2.merged.bam
symlink_readset_sample_bam.CKIIts_37C_Cl2.7893590e47fd87ad9e01ec7afa45898a.mugqic.done
)
picard_merge_sam_files_8_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_merge_sam_files_8_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_9_JOB_ID: symlink_readset_sample_bam.WT_37C_Cl1
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.WT_37C_Cl1
JOB_DEPENDENCIES=$samtools_view_filter_9_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.WT_37C_Cl1.f59ed50e456bb7532a9215480c26887f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.WT_37C_Cl1.f59ed50e456bb7532a9215480c26887f.mugqic.done'
mkdir -p alignment/WT_37C_Cl1 && \
ln -s -f HI.4594.004.Index_13.Mnase_Exp3_WT37C2h_Cl1/HI.4594.004.Index_13.Mnase_Exp3_WT37C2h_Cl1.sorted.filtered.bam alignment/WT_37C_Cl1/WT_37C_Cl1.merged.bam
symlink_readset_sample_bam.WT_37C_Cl1.f59ed50e456bb7532a9215480c26887f.mugqic.done
)
picard_merge_sam_files_9_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_merge_sam_files_9_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_10_JOB_ID: symlink_readset_sample_bam.WT_37C_Cl2
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.WT_37C_Cl2
JOB_DEPENDENCIES=$samtools_view_filter_10_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.WT_37C_Cl2.66bc53ae4cbc636bc1804a2c85bc5b3a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.WT_37C_Cl2.66bc53ae4cbc636bc1804a2c85bc5b3a.mugqic.done'
mkdir -p alignment/WT_37C_Cl2 && \
ln -s -f HI.4594.004.Index_6.Mnase_Exp3_WT37C2h_Cl2/HI.4594.004.Index_6.Mnase_Exp3_WT37C2h_Cl2.sorted.filtered.bam alignment/WT_37C_Cl2/WT_37C_Cl2.merged.bam
symlink_readset_sample_bam.WT_37C_Cl2.66bc53ae4cbc636bc1804a2c85bc5b3a.mugqic.done
)
picard_merge_sam_files_10_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_merge_sam_files_10_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_11_JOB_ID: symlink_readset_sample_bam.spt6ts_39C_Cl1
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.spt6ts_39C_Cl1
JOB_DEPENDENCIES=$samtools_view_filter_11_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.spt6ts_39C_Cl1.ea695af703378ea579ed3eee27b20747.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.spt6ts_39C_Cl1.ea695af703378ea579ed3eee27b20747.mugqic.done'
mkdir -p alignment/spt6ts_39C_Cl1 && \
ln -s -f HI.4594.004.Index_11.Mnase_spt6ts_YAR844_39C1h_Cl1/HI.4594.004.Index_11.Mnase_spt6ts_YAR844_39C1h_Cl1.sorted.filtered.bam alignment/spt6ts_39C_Cl1/spt6ts_39C_Cl1.merged.bam
symlink_readset_sample_bam.spt6ts_39C_Cl1.ea695af703378ea579ed3eee27b20747.mugqic.done
)
picard_merge_sam_files_11_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_merge_sam_files_11_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_12_JOB_ID: symlink_readset_sample_bam.spt6ts_39C_Cl2
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.spt6ts_39C_Cl2
JOB_DEPENDENCIES=$samtools_view_filter_12_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.spt6ts_39C_Cl2.657835502cd6bc2e26ef8d8a937bb6ca.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.spt6ts_39C_Cl2.657835502cd6bc2e26ef8d8a937bb6ca.mugqic.done'
mkdir -p alignment/spt6ts_39C_Cl2 && \
ln -s -f HI.4594.004.Index_20.Mnase_spt6ts_YAR844_39C1h_Cl2/HI.4594.004.Index_20.Mnase_spt6ts_YAR844_39C1h_Cl2.sorted.filtered.bam alignment/spt6ts_39C_Cl2/spt6ts_39C_Cl2.merged.bam
symlink_readset_sample_bam.spt6ts_39C_Cl2.657835502cd6bc2e26ef8d8a937bb6ca.mugqic.done
)
picard_merge_sam_files_12_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_merge_sam_files_12_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_13_JOB_ID: symlink_readset_sample_bam.WT_39C_Cl1
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.WT_39C_Cl1
JOB_DEPENDENCIES=$samtools_view_filter_13_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.WT_39C_Cl1.889314d90eba604ad2193fe959c938d6.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.WT_39C_Cl1.889314d90eba604ad2193fe959c938d6.mugqic.done'
mkdir -p alignment/WT_39C_Cl1 && \
ln -s -f HI.4594.004.Index_8.Mnase_WT_YAR845_39C1h_Cl1/HI.4594.004.Index_8.Mnase_WT_YAR845_39C1h_Cl1.sorted.filtered.bam alignment/WT_39C_Cl1/WT_39C_Cl1.merged.bam
symlink_readset_sample_bam.WT_39C_Cl1.889314d90eba604ad2193fe959c938d6.mugqic.done
)
picard_merge_sam_files_13_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_merge_sam_files_13_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_14_JOB_ID: symlink_readset_sample_bam.WT_39C_Cl3
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.WT_39C_Cl3
JOB_DEPENDENCIES=$samtools_view_filter_14_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.WT_39C_Cl3.9f5a9c84613e68e441d1e89870b3267d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.WT_39C_Cl3.9f5a9c84613e68e441d1e89870b3267d.mugqic.done'
mkdir -p alignment/WT_39C_Cl3 && \
ln -s -f HI.4594.004.Index_10.Mnase_WT_YAR845_39C1h_Cl3/HI.4594.004.Index_10.Mnase_WT_YAR845_39C1h_Cl3.sorted.filtered.bam alignment/WT_39C_Cl3/WT_39C_Cl3.merged.bam
symlink_readset_sample_bam.WT_39C_Cl3.9f5a9c84613e68e441d1e89870b3267d.mugqic.done
)
picard_merge_sam_files_14_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_merge_sam_files_14_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# STEP: picard_mark_duplicates
#-------------------------------------------------------------------------------
STEP=picard_mark_duplicates
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_1_JOB_ID: picard_mark_duplicates.dspt2_30C_Cl2
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.dspt2_30C_Cl2
JOB_DEPENDENCIES=$picard_merge_sam_files_1_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.dspt2_30C_Cl2.37f80c1ddd92508e11d779d00017e583.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.dspt2_30C_Cl2.37f80c1ddd92508e11d779d00017e583.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/2.0.1 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx5G -jar $PICARD_HOME/picard.jar MarkDuplicates \
 REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/dspt2_30C_Cl2/dspt2_30C_Cl2.merged.bam \
 OUTPUT=alignment/dspt2_30C_Cl2/dspt2_30C_Cl2.sorted.dup.bam \
 METRICS_FILE=alignment/dspt2_30C_Cl2/dspt2_30C_Cl2.sorted.dup.metrics \
 MAX_RECORDS_IN_RAM=1000000
picard_mark_duplicates.dspt2_30C_Cl2.37f80c1ddd92508e11d779d00017e583.mugqic.done
)
picard_mark_duplicates_1_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=8G -N 1 -n 2 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_mark_duplicates_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_2_JOB_ID: picard_mark_duplicates.dspt2_30C_Cl3
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.dspt2_30C_Cl3
JOB_DEPENDENCIES=$picard_merge_sam_files_2_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.dspt2_30C_Cl3.1f2eb248d3f127e0da4f04be07afde08.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.dspt2_30C_Cl3.1f2eb248d3f127e0da4f04be07afde08.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/2.0.1 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx5G -jar $PICARD_HOME/picard.jar MarkDuplicates \
 REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/dspt2_30C_Cl3/dspt2_30C_Cl3.merged.bam \
 OUTPUT=alignment/dspt2_30C_Cl3/dspt2_30C_Cl3.sorted.dup.bam \
 METRICS_FILE=alignment/dspt2_30C_Cl3/dspt2_30C_Cl3.sorted.dup.metrics \
 MAX_RECORDS_IN_RAM=1000000
picard_mark_duplicates.dspt2_30C_Cl3.1f2eb248d3f127e0da4f04be07afde08.mugqic.done
)
picard_mark_duplicates_2_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=8G -N 1 -n 2 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_mark_duplicates_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_3_JOB_ID: picard_mark_duplicates.spt2m_30C_Cl2
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.spt2m_30C_Cl2
JOB_DEPENDENCIES=$picard_merge_sam_files_3_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.spt2m_30C_Cl2.4916264de7b2a2e2508e969d165a942b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.spt2m_30C_Cl2.4916264de7b2a2e2508e969d165a942b.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/2.0.1 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx5G -jar $PICARD_HOME/picard.jar MarkDuplicates \
 REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/spt2m_30C_Cl2/spt2m_30C_Cl2.merged.bam \
 OUTPUT=alignment/spt2m_30C_Cl2/spt2m_30C_Cl2.sorted.dup.bam \
 METRICS_FILE=alignment/spt2m_30C_Cl2/spt2m_30C_Cl2.sorted.dup.metrics \
 MAX_RECORDS_IN_RAM=1000000
picard_mark_duplicates.spt2m_30C_Cl2.4916264de7b2a2e2508e969d165a942b.mugqic.done
)
picard_mark_duplicates_3_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=8G -N 1 -n 2 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_mark_duplicates_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_4_JOB_ID: picard_mark_duplicates.spt2m_30C_Cl3
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.spt2m_30C_Cl3
JOB_DEPENDENCIES=$picard_merge_sam_files_4_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.spt2m_30C_Cl3.6cf862694620f27732195412f7e01121.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.spt2m_30C_Cl3.6cf862694620f27732195412f7e01121.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/2.0.1 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx5G -jar $PICARD_HOME/picard.jar MarkDuplicates \
 REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/spt2m_30C_Cl3/spt2m_30C_Cl3.merged.bam \
 OUTPUT=alignment/spt2m_30C_Cl3/spt2m_30C_Cl3.sorted.dup.bam \
 METRICS_FILE=alignment/spt2m_30C_Cl3/spt2m_30C_Cl3.sorted.dup.metrics \
 MAX_RECORDS_IN_RAM=1000000
picard_mark_duplicates.spt2m_30C_Cl3.6cf862694620f27732195412f7e01121.mugqic.done
)
picard_mark_duplicates_4_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=8G -N 1 -n 2 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_mark_duplicates_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_5_JOB_ID: picard_mark_duplicates.WT_30C_Cl2
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.WT_30C_Cl2
JOB_DEPENDENCIES=$picard_merge_sam_files_5_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.WT_30C_Cl2.1df0f7113001f8913528cfa7ded417c6.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.WT_30C_Cl2.1df0f7113001f8913528cfa7ded417c6.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/2.0.1 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx5G -jar $PICARD_HOME/picard.jar MarkDuplicates \
 REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/WT_30C_Cl2/WT_30C_Cl2.merged.bam \
 OUTPUT=alignment/WT_30C_Cl2/WT_30C_Cl2.sorted.dup.bam \
 METRICS_FILE=alignment/WT_30C_Cl2/WT_30C_Cl2.sorted.dup.metrics \
 MAX_RECORDS_IN_RAM=1000000
picard_mark_duplicates.WT_30C_Cl2.1df0f7113001f8913528cfa7ded417c6.mugqic.done
)
picard_mark_duplicates_5_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=8G -N 1 -n 2 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_mark_duplicates_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_6_JOB_ID: picard_mark_duplicates.WT_30C_Cl3
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.WT_30C_Cl3
JOB_DEPENDENCIES=$picard_merge_sam_files_6_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.WT_30C_Cl3.88fd9908c8da31a5a441ae96f5955284.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.WT_30C_Cl3.88fd9908c8da31a5a441ae96f5955284.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/2.0.1 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx5G -jar $PICARD_HOME/picard.jar MarkDuplicates \
 REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/WT_30C_Cl3/WT_30C_Cl3.merged.bam \
 OUTPUT=alignment/WT_30C_Cl3/WT_30C_Cl3.sorted.dup.bam \
 METRICS_FILE=alignment/WT_30C_Cl3/WT_30C_Cl3.sorted.dup.metrics \
 MAX_RECORDS_IN_RAM=1000000
picard_mark_duplicates.WT_30C_Cl3.88fd9908c8da31a5a441ae96f5955284.mugqic.done
)
picard_mark_duplicates_6_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=8G -N 1 -n 2 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_mark_duplicates_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_7_JOB_ID: picard_mark_duplicates.CKIIts_37C_Cl1
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.CKIIts_37C_Cl1
JOB_DEPENDENCIES=$picard_merge_sam_files_7_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.CKIIts_37C_Cl1.2c2a7abe22aac4db32daf76dffeffd24.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.CKIIts_37C_Cl1.2c2a7abe22aac4db32daf76dffeffd24.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/2.0.1 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx5G -jar $PICARD_HOME/picard.jar MarkDuplicates \
 REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/CKIIts_37C_Cl1/CKIIts_37C_Cl1.merged.bam \
 OUTPUT=alignment/CKIIts_37C_Cl1/CKIIts_37C_Cl1.sorted.dup.bam \
 METRICS_FILE=alignment/CKIIts_37C_Cl1/CKIIts_37C_Cl1.sorted.dup.metrics \
 MAX_RECORDS_IN_RAM=1000000
picard_mark_duplicates.CKIIts_37C_Cl1.2c2a7abe22aac4db32daf76dffeffd24.mugqic.done
)
picard_mark_duplicates_7_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=8G -N 1 -n 2 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_mark_duplicates_7_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_8_JOB_ID: picard_mark_duplicates.CKIIts_37C_Cl2
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.CKIIts_37C_Cl2
JOB_DEPENDENCIES=$picard_merge_sam_files_8_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.CKIIts_37C_Cl2.ef5d12b328e3d47db81836cd48dd81b7.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.CKIIts_37C_Cl2.ef5d12b328e3d47db81836cd48dd81b7.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/2.0.1 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx5G -jar $PICARD_HOME/picard.jar MarkDuplicates \
 REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/CKIIts_37C_Cl2/CKIIts_37C_Cl2.merged.bam \
 OUTPUT=alignment/CKIIts_37C_Cl2/CKIIts_37C_Cl2.sorted.dup.bam \
 METRICS_FILE=alignment/CKIIts_37C_Cl2/CKIIts_37C_Cl2.sorted.dup.metrics \
 MAX_RECORDS_IN_RAM=1000000
picard_mark_duplicates.CKIIts_37C_Cl2.ef5d12b328e3d47db81836cd48dd81b7.mugqic.done
)
picard_mark_duplicates_8_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=8G -N 1 -n 2 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_mark_duplicates_8_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_9_JOB_ID: picard_mark_duplicates.WT_37C_Cl1
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.WT_37C_Cl1
JOB_DEPENDENCIES=$picard_merge_sam_files_9_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.WT_37C_Cl1.2131bae1fc5ff89db1f9725f9d4e42f4.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.WT_37C_Cl1.2131bae1fc5ff89db1f9725f9d4e42f4.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/2.0.1 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx5G -jar $PICARD_HOME/picard.jar MarkDuplicates \
 REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/WT_37C_Cl1/WT_37C_Cl1.merged.bam \
 OUTPUT=alignment/WT_37C_Cl1/WT_37C_Cl1.sorted.dup.bam \
 METRICS_FILE=alignment/WT_37C_Cl1/WT_37C_Cl1.sorted.dup.metrics \
 MAX_RECORDS_IN_RAM=1000000
picard_mark_duplicates.WT_37C_Cl1.2131bae1fc5ff89db1f9725f9d4e42f4.mugqic.done
)
picard_mark_duplicates_9_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=8G -N 1 -n 2 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_mark_duplicates_9_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_10_JOB_ID: picard_mark_duplicates.WT_37C_Cl2
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.WT_37C_Cl2
JOB_DEPENDENCIES=$picard_merge_sam_files_10_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.WT_37C_Cl2.b9eacce221c6899ef9836205fb9e2da4.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.WT_37C_Cl2.b9eacce221c6899ef9836205fb9e2da4.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/2.0.1 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx5G -jar $PICARD_HOME/picard.jar MarkDuplicates \
 REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/WT_37C_Cl2/WT_37C_Cl2.merged.bam \
 OUTPUT=alignment/WT_37C_Cl2/WT_37C_Cl2.sorted.dup.bam \
 METRICS_FILE=alignment/WT_37C_Cl2/WT_37C_Cl2.sorted.dup.metrics \
 MAX_RECORDS_IN_RAM=1000000
picard_mark_duplicates.WT_37C_Cl2.b9eacce221c6899ef9836205fb9e2da4.mugqic.done
)
picard_mark_duplicates_10_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=8G -N 1 -n 2 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_mark_duplicates_10_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_11_JOB_ID: picard_mark_duplicates.spt6ts_39C_Cl1
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.spt6ts_39C_Cl1
JOB_DEPENDENCIES=$picard_merge_sam_files_11_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.spt6ts_39C_Cl1.3d523e0f5ef3a23eef0ffcd1bd53b77a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.spt6ts_39C_Cl1.3d523e0f5ef3a23eef0ffcd1bd53b77a.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/2.0.1 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx5G -jar $PICARD_HOME/picard.jar MarkDuplicates \
 REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/spt6ts_39C_Cl1/spt6ts_39C_Cl1.merged.bam \
 OUTPUT=alignment/spt6ts_39C_Cl1/spt6ts_39C_Cl1.sorted.dup.bam \
 METRICS_FILE=alignment/spt6ts_39C_Cl1/spt6ts_39C_Cl1.sorted.dup.metrics \
 MAX_RECORDS_IN_RAM=1000000
picard_mark_duplicates.spt6ts_39C_Cl1.3d523e0f5ef3a23eef0ffcd1bd53b77a.mugqic.done
)
picard_mark_duplicates_11_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=8G -N 1 -n 2 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_mark_duplicates_11_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_12_JOB_ID: picard_mark_duplicates.spt6ts_39C_Cl2
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.spt6ts_39C_Cl2
JOB_DEPENDENCIES=$picard_merge_sam_files_12_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.spt6ts_39C_Cl2.be3918be4af1bd9c7cbd22677d13551e.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.spt6ts_39C_Cl2.be3918be4af1bd9c7cbd22677d13551e.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/2.0.1 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx5G -jar $PICARD_HOME/picard.jar MarkDuplicates \
 REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/spt6ts_39C_Cl2/spt6ts_39C_Cl2.merged.bam \
 OUTPUT=alignment/spt6ts_39C_Cl2/spt6ts_39C_Cl2.sorted.dup.bam \
 METRICS_FILE=alignment/spt6ts_39C_Cl2/spt6ts_39C_Cl2.sorted.dup.metrics \
 MAX_RECORDS_IN_RAM=1000000
picard_mark_duplicates.spt6ts_39C_Cl2.be3918be4af1bd9c7cbd22677d13551e.mugqic.done
)
picard_mark_duplicates_12_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=8G -N 1 -n 2 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_mark_duplicates_12_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_13_JOB_ID: picard_mark_duplicates.WT_39C_Cl1
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.WT_39C_Cl1
JOB_DEPENDENCIES=$picard_merge_sam_files_13_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.WT_39C_Cl1.c77811bd8ebc94b3f16e05af595720ff.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.WT_39C_Cl1.c77811bd8ebc94b3f16e05af595720ff.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/2.0.1 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx5G -jar $PICARD_HOME/picard.jar MarkDuplicates \
 REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/WT_39C_Cl1/WT_39C_Cl1.merged.bam \
 OUTPUT=alignment/WT_39C_Cl1/WT_39C_Cl1.sorted.dup.bam \
 METRICS_FILE=alignment/WT_39C_Cl1/WT_39C_Cl1.sorted.dup.metrics \
 MAX_RECORDS_IN_RAM=1000000
picard_mark_duplicates.WT_39C_Cl1.c77811bd8ebc94b3f16e05af595720ff.mugqic.done
)
picard_mark_duplicates_13_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=8G -N 1 -n 2 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_mark_duplicates_13_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_14_JOB_ID: picard_mark_duplicates.WT_39C_Cl3
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.WT_39C_Cl3
JOB_DEPENDENCIES=$picard_merge_sam_files_14_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.WT_39C_Cl3.cabdc8b6bb6dc2a0475f48fc771d5f27.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.WT_39C_Cl3.cabdc8b6bb6dc2a0475f48fc771d5f27.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/2.0.1 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.use_async_io=true -Dsamjdk.buffer_size=4194304 -Xmx5G -jar $PICARD_HOME/picard.jar MarkDuplicates \
 REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/WT_39C_Cl3/WT_39C_Cl3.merged.bam \
 OUTPUT=alignment/WT_39C_Cl3/WT_39C_Cl3.sorted.dup.bam \
 METRICS_FILE=alignment/WT_39C_Cl3/WT_39C_Cl3.sorted.dup.metrics \
 MAX_RECORDS_IN_RAM=1000000
picard_mark_duplicates.WT_39C_Cl3.cabdc8b6bb6dc2a0475f48fc771d5f27.mugqic.done
)
picard_mark_duplicates_14_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=8G -N 1 -n 2 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_mark_duplicates_14_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_15_JOB_ID: picard_mark_duplicates_report
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates_report
JOB_DEPENDENCIES=$picard_mark_duplicates_1_JOB_ID:$picard_mark_duplicates_2_JOB_ID:$picard_mark_duplicates_3_JOB_ID:$picard_mark_duplicates_4_JOB_ID:$picard_mark_duplicates_5_JOB_ID:$picard_mark_duplicates_6_JOB_ID:$picard_mark_duplicates_7_JOB_ID:$picard_mark_duplicates_8_JOB_ID:$picard_mark_duplicates_9_JOB_ID:$picard_mark_duplicates_10_JOB_ID:$picard_mark_duplicates_11_JOB_ID:$picard_mark_duplicates_12_JOB_ID:$picard_mark_duplicates_13_JOB_ID:$picard_mark_duplicates_14_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates_report.0d6756c3b0268d30a7f6ff885850a9fd.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates_report.0d6756c3b0268d30a7f6ff885850a9fd.mugqic.done'
mkdir -p report && \
cp \
  /cvmfs/soft.mugqic/CentOS6/software/genpipes/genpipes-3.1.2/bfx/report/ChipSeq.picard_mark_duplicates.md \
  report/ChipSeq.picard_mark_duplicates.md
picard_mark_duplicates_report.0d6756c3b0268d30a7f6ff885850a9fd.mugqic.done
)
picard_mark_duplicates_15_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_mark_duplicates_15_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.2


#-------------------------------------------------------------------------------
# Call home with pipeline statistics
#-------------------------------------------------------------------------------
LOG_MD5=$(echo $USER-'206.12.124.6-ChipSeq-dspt2_30C_Cl3.HI.4594.004.Index_25.Mnase_dspt2_YAR669_Cl3,dspt2_30C_Cl2.HI.4594.004.Index_22.Mnase_dspt2_YAR669_Cl2,WT_30C_Cl2.HI.4594.004.Index_21.Mnase_WT_YAR670_Cl2,WT_30C_Cl3.HI.4594.004.Index_23.Mnase_WT_YAR670_Cl3,spt2m_30C_Cl2.HI.4594.004.Index_27.Mnase_spt2m_YAR673_Cl2,spt2m_30C_Cl3.HI.4594.004.Index_2.Mnase_spt2m_YAR673_Cl3,spt6ts_39C_Cl1.HI.4594.004.Index_11.Mnase_spt6ts_YAR844_39C1h_Cl1,WT_37C_Cl1.HI.4594.004.Index_13.Mnase_Exp3_WT37C2h_Cl1,WT_37C_Cl2.HI.4594.004.Index_6.Mnase_Exp3_WT37C2h_Cl2,WT_39C_Cl3.HI.4594.004.Index_10.Mnase_WT_YAR845_39C1h_Cl3,spt6ts_39C_Cl2.HI.4594.004.Index_20.Mnase_spt6ts_YAR844_39C1h_Cl2,WT_39C_Cl1.HI.4594.004.Index_8.Mnase_WT_YAR845_39C1h_Cl1,CKIIts_37C_Cl2.HI.4594.004.Index_7.Mnase_Exp3_CKIIts_37C2h_Cl2,CKIIts_37C_Cl1.HI.4594.004.Index_15.Mnase_Exp3_CKIIts_37C2h_Cl1' | md5sum | awk '{ print $1 }')
wget "http://mugqic.hpc.mcgill.ca/cgi-bin/pipeline.cgi?hostname=cedar5.cedar.computecanada.ca&ip=206.12.124.6&pipeline=ChipSeq&steps=picard_sam_to_fastq,trimmomatic,merge_trimmomatic_stats,bwa_mem_picard_sort_sam,samtools_view_filter,picard_merge_sam_files,picard_mark_duplicates&samples=14&md5=$LOG_MD5" --quiet --output-document=/dev/null

