#!/bin/bash
#SBATCH --time=12:00:00
#SBATCH --cpus-per-task=8
#SBATCH --mem=128G
#SBATCH --account=def-amnou
#SBATCH --chdir /project/6007406/efournie/Mnase
#SBATCH --error identify_ndr.stderr
#SBATCH --output identify_ndr.stdout


module load r
Rscript scripts/IdentifyNDR.R