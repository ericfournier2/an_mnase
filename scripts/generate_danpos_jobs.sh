#!/bin/bash

# First, get a list of all bam categories (Strain + Temp)
for i in output/danpos_input_filter/*.bam ; 
do 
    basename $i .bam | sed -e 's/_Cl.$//'; 
done > tmp;

# Then, build directories where all the bams from the same category are linked
dt=$(date '+%Y-%m-%dGMT%H:%M:%S');
mkdir -p danpos_input_auto/$dt
for i in `uniq tmp` ;
do
    mkdir -p danpos_input_auto/$dt/$i
    #for j in output/pipeline/alignment/$i*/$i*.ba* ;
    for j in output/danpos_input_filter/$i*.ba* ;
    do
        ln -s `pwd`/$j danpos_input_auto/$dt/$i
    done
done

# Finally, run danpos the correct pairs of directories.
mkdir -p output/jobs
while read l; do
    t=`echo $l | cut -d' ' -f 1`
    c=`echo $l | cut -d' ' -f 2`
    comp="$t:$c"
    out="$t"_vs_"$c"
    script=$out.danpos.sh
    scriptpath=output/jobs/$script
cat <<EOF > $scriptpath
#!/bin/bash
module load samtools/0.1.17
module load r

mkdir -p $out
python ~/danpos-2.2.2/danpos.py dpos \
    $comp \
    -a 5 -q 25 -t 0.05 -f 1 -m 1 -o $out
EOF
    
    sbatch -D danpos_input_auto/$dt -o $script.stdout -e $script.stderr --time 8:00:00 --mem 32G --account def-amnou --cpus-per-task 1 $scriptpath
done < input/danpos_comparisons.txt

